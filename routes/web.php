<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale}', function ($locale) {
    session(['locale' =>  $locale]);
    session()->keep('success');   
    return redirect()->back();
});

Route::get('/', function () {
    return redirect('/dashboard');
});

Auth::routes(['verify' => true]);

Route::middleware('admin_role')->group(function () {
	Route::resource('users', 'UserController', ['names' => 'users']);
	Route::resource('article_categories', 'ArticleCategoryController', ['names' => 'article_categories']);
	Route::resource('push_messages', 'PushMessageController', ['names' => 'push_messages']);
});

Route::middleware('verified','user_role',)->group(function () {
	Route::get('/dashboard', 'UserDashboardController@index')->name('user_dashboard');
	Route::resource('customers', 'CustomerController', ['names' => 'customers']);
	Route::resource('articles', 'ArticleController', ['names' => 'articles']);
	Route::resource('offers', 'OfferController', ['names' => 'offers']);
	Route::resource('invoices', 'InvoiceController', ['names' => 'invoices']);
	Route::resource('user-settings', 'UserSettingsController', ['names' => 'user-settings']);
	Route::resource('offer-invoice-settings', 'OfferInvoiceSettingsController', ['names' => 'offer-invoice-settings']);
	Route::post('get-article-data/{id}','OfferController@getArticleData');
	Route::get('/help', 'UserHelpController@index')->name('help');
});
