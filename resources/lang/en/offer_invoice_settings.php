<?php

return [
	'offer_invoice_settings' => 'Offer & Invoice Settings',
	'offer_settings' => 'Offer Settings',
	'offer_prefix' => 'Offer number prefix',
	'offer_suffix' => 'Offer number sufix',
	'offer_delimiter' => 'Offer number delimiter',
	'invoice_settings' => 'Invoice Settings',
	'invoice_prefix' => 'Invoice number prefix',
	'invoice_suffix' => 'Invoice number sufix',
	'invoice_delimiter' => 'Invoice number delimiter',
	'example' => 'Example',
	'settings_saved' => 'Settings saved successfully.',
];
