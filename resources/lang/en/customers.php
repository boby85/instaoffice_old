<?php

return [

	'customer_number' => 'Customer Number',
	'customers' => 'Customers',
	'add_customer' => 'Add Customer',
	'company_name' => 'Company Name',
	'title' => 'Salutation',
	'mr' => 'Mr',
	'ms' => 'Ms',
	'name' => 'Name',
	'phone' => 'Phone',
	'mobile_phone' => 'Mobile phone',
	'address' => 'Address',
	'city' => 'City',
	'zip_code' => 'Zip code',
	'country' => 'Country',
	'email_address' => 'E-Mail Address',
	'company_tax_number' => 'Tax number',
	'customer_details' => 'Customer Details',
	'edit_customer' => 'Edit Customer',
	'customer_added' => 'Customer added successfully.',
	'customer_updated' => 'Customer updated successfully.',
	'customer_deleted' => 'Customer deleted successfully.',
	'no_customers' => 'You don\'t have any customers.',
	'customer_invoices' => 'Customer invoices',
	'customer_offers' => 'Customer offers'
];