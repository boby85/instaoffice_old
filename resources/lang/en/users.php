<?php

return [
	'users' => 'Users',
	'add_user' => 'Add user',
	'company_name' => 'Company Name',
	'name' => 'Name',
	'phone' => 'Phone',
	'address' => 'Address',
	'zip_code' => 'Zip code',
	'city' => 'City',
	'country' => 'Country',
	'status' => 'Status',
	'role' => 'Role',
	'user_added' => 'User added successfully.',
	'user_details' => 'User details',
	'edit_user' => 'Edit user',
	'company_name' => 'Company name',
	'user_updated' => 'User updated successfully.',
	'user_deleted' => 'User deleted successfully.'
];