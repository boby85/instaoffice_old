<?php

return [
	'article_categories' => 'Article categories',
	'no_article_categories' => 'No article categories',
	'category_name' => 'Category name',
	'category_id' => 'Category ID',
	'category_parent_id' => 'Category Parent ID',
	'add_category' => 'Add category',
	'article_category_added' => 'Category added.',
	'edit_category' => 'Edit category',
	'category_updated' => 'Category updated.',
	'category_deleted' => 'Category deleted.'
];