<?php

return [

	'articles' => 'Articles',
	'add_article' => 'Add Article',
	'no_articles' => 'You don\'t have any articles.',
	'price' => 'Price',
	'currency' => 'Currency',
	'article_details' => 'Article Details',
	'description' => 'Description',
	'unit' => 'Unit',
	'price_unit' => 'Price unit',
	'piece' => 'Piece',
	'kg' => 'Kilogram',
	'article_image' => 'Article image',
	'edit_article' => 'Edit Article',
	'article_added' => 'Article added successfully.',
	'article_updated' => 'Article updated successfully.',
	'article_deleted' => 'Article deleted successfully.',
	
];