<?php

return [
	'help' => 'Help',
	'help_message' => 'For any application related help, plase contact us at support@instaoffice.eu.'
];