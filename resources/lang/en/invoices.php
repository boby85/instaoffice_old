<?php

return [

	'invoice' => 'Invoice',
	'invoices' => 'Invoices',
	'add_invoice' => 'Add invoice',
	'no_invoices' => 'You don\'t have any invoices.',
	'customer_no_invoices' => 'No invoices for this customer.',
	'no_customers' => 'You don\'t have any customers! Use menu on the left to add new customer.',
	'no_articles' => 'You don\'t have any articles! Use menu on the left to add new article.',
	'invoice_added' => 'Invoice created successfully.',
	'invoice_updated' => 'Invoice updated successfully.',
	'invoice_deleted' => 'Invoice deleted successfully.',
	'invoice_number' => 'Invoice number',
	'customer' => 'Customer',
	'select_customer' => 'Select customer',
	'date' => 'Invoice date',
	'article' => 'Article',
	'select_article' => 'Select article',
	'price' => 'Price',
	'quantity' => 'Quantity',
	'discount' => 'Discount',
	'total' => 'Total',
	'tax' => 'Tax',
	'grand_total' => 'Grand Total',
	'edit_invoice' => 'Edit Invoice',
	'tax_free_note' => 'Note: Transfer of VAT liability in accordance with Section 19 (1a) UstG',
	'page' => 'Page'
];