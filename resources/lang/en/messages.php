<?php

return [

	'confirm_delete' => 'Delete Confirmation',
	'sure_to_delete' => 'Are you sure you want to delete?',
	'check_form' => 'Please check the form below for errors',
	'no_new_messages' => 'You don\'t have any new messages.',
	'custom_insert' => 'Custom insert',
	'edit_article' => 'Edit article'
];

