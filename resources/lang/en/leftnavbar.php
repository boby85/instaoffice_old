<?php

return [
    'dashboard' => 'Dashboard',
    'customers' => 'Customers',
    'articles' => 'Articles',
    'article_categories' => 'Article categories',
    'invoices' => 'Invoices',
    'settings' => 'Settings',
    'user-settings' => 'User Settings',
    'offers' => 'Offers',
	'offers-invoices' => 'Offers & Invoices',
	'help' => 'Help',
	'users' => 'Users',
    'push_messages' => 'Push messages'
];