<?php

return [

	'no_new_messages' => 'You don\'t have any new messages.',
	'welcome_message_title' => 'Welcome to instaoffice.eu',
	'welcome_message_1' => 'Please use menu on the left to get familiar with the application. If you have any questions, please contact us at support@instaoffice.eu.',
	'welcome_message_2' => 'We hope that you will enjoy using our application.'
];