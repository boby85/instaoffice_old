<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email_address' => 'E-Mail Address',
    'password' => 'Password',
    'login' => 'Login',
    'register' => 'Register',
    'remember_me' => 'Remember me',
    'forgot_password?' => 'Forgot Your Password?',
    'logout' => 'Logout',
    'reset_password' => 'Reset Password',
    'confirm_password' => 'Confirm Password',
    'confirm_password_message' => 'Please confirm your password before continuing.',
    'create_new_account' => 'Create new account',
    'company_name' => 'Company Name',
    'name' => 'Name',
    'phone' => 'Phone',
    'address' => 'Address',
    'zip_code' => 'Zip code',
    'city' => 'City',
    'country' => 'Country',
    'verify_email_header' => 'Verify Your Email Address',
    'fresh_link_send_to_email' => 'A fresh verification link has been sent to your email address.',
    'check_your_email_for_verification' => 'Before proceeding, please check your email for a verification link.',
    'did_not_receive_email' => 'If you did not receive the email,',
    'click_to_request_another' => 'click here to request another.',
    'account_inactive' => 'Your account is deactivated. Please contact your administrator.'
];
