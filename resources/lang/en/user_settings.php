<?php

return [

	'general_settings' => 'General Settings',
	'company_details' => 'Company Details:',
	'company_name' => 'Company Name',
	'name' => 'Name',
	'first_name' => 'First Name',
	'last_name' => 'Last Name',
	'address' => 'Address',
	'city' => 'City',
	'zip' => 'Zip',
	'country' => 'Country',
	'phone' => 'Phone',
	'company_uid' => 'Company UID',
	'company_tax_number' => 'Tax number',
	'company_register_number' => 'Register number',
	'company_logo' => 'Company Logo',
	'bank_details' => 'Bank details:',
	'other' => 'Other:',
	'language' => 'Language',
	'settings_saved' => 'Settings saved successfully.',
];