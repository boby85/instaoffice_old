<?php

return [

	'back' => 'Back',
	'save' => 'Save',
	'update' => 'Update',
	'cancel' => 'Cancel',
	'delete' => 'Delete',
	'add' => 'Add',
	'manual_input' => 'Manual input',
	'preview' => 'Preview',
	'login' => 'Login',
	'register' => 'Register',
	'send_password_reset_link' => 'Send Password Reset Link',
	'reset_password' => 'Reset Password',
	'confirm_password' => 'Confirm Password',
	'deactivate_user' => 'Deactivate user',
	'activate_user' => 'Activate user',
];