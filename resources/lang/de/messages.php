<?php

return [

	'confirm_delete' => 'Bestätigung löschen',
	'sure_to_delete' => 'Sind Sie sicher, dass Sie löschen möchten?',
	'check_form' => 'Bitte überprüfen Sie das Formular unten auf Fehler',
	'no_new_messages' => 'Sie haben keine neuen Nachrichten.',
	'custom_insert' => 'Benutzerdefinierte Beilage',
	'edit_article' => 'Artikel bearbeiten'
];