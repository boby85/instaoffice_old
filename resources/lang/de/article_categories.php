<?php

return [
	'article_categories' => 'Artikelkategorien',
	'no_article_categories' => 'Keine Artikelkategorien',
	'category_name' => 'Kategoriename',
	'category_id' => 'Kategorie ID',
	'category_parent_id' => 'Kategorie Eltern-ID',
	'add_category' => 'Kategorie hinzufügen',
	'article_category_added' => 'Kategorie hinzugefügt.',
	'edit_category' => 'Kategorie bearbeiten',
	'category_updated' => 'Kategorie aktualisiert.',
	'category_deleted' => 'Kategorie gelöscht.'
];