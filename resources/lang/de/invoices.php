<?php

return [

	'invoice' => 'Rehnung',
	'invoices' => 'Rechnungen',
	'add_invoice' => 'Rechnung hinzufügen',
	'no_invoices' => 'Sie haben keine Rechnungen.',
	'customer_no_invoices' => 'Keine Rechnungen für diesen Kunden.',
	'no_customers' => 'Sie haben keine Kunden! Verwenden Sie das Menü links, um einen neuen Kunden hinzuzufügen.',
	'no_articles' => 'Sie haben keine Artikel! Verwenden Sie das Menü links, um einen neuen Artikel hinzuzufügen.',
	'invoice_added' => 'Rechnung erfolgreich erstellt.',
	'invoice_updated' => 'Rechnung erfolgreich aktualisiert.',
	'invoice_deleted' => 'Rechnung erfolgreich gelöscht.',
	'invoice_number' => 'Rechnungsnummer',
	'customer' => 'Kunde',
	'select_customer' => 'Kunde auswählen',
	'date' => 'Rechnungsdatum',
	'article' => 'Artikel',
	'select_article' => 'Artikel auswählen',
	'price' => 'Preis',
	'quantity' => 'Menge',
	'discount' => 'Rabatt',
	'total' => 'Gesamt',
	'tax' => 'MwSt',
	'grand_total' => 'Gesamtsumme',
	'edit_invoice' => 'Rechnung bearbeiten',
	'pdf_not_found' => 'PDF-Datei nicht gefunden! Klicken Sie auf Aktualisieren, um die Datei neu zu erstellen.',
	'tax_free_note' => 'Hinweis: Übergang der Ust-Schuld gemäß § 19 Abs. 1a UstG',
	'page' => 'Seite'
];