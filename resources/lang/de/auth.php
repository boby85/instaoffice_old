<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Diese Anmeldeinformationen stimmen nicht mit unseren Aufzeichnungen überein.',
    'throttle' => 'Zu viele Anmeldeversuche. Bitte versuchen Sie es in :seconds Sekunden erneut.',
    'email_address' => 'E-Mail-Addresse',
    'password' => 'Passwort',
    'login' => 'Anmeldung',
    'register' => 'Registrieren',
    'remember_me' => 'Angemeldet bleiben',
    'forgot_password?' => 'Passwort vergessen?',
    'logout' => 'Abmelden',
    'reset_password' => 'Passwort zurücksetzen',
    'confirm_password' => 'Passwort bestätigen',
    'confirm_password_message' => 'Bitte bestätigen Sie Ihr Passwort, bevor Sie fortfahren.',
    'create_new_account' => 'Neue Konto erstellen',
    'company_name' => 'Firmenname',
    'name' => 'Name',
    'phone' => 'Telefon',
    'address' => 'Adresse',
    'zip_code' => 'Postleitzahl',
    'city' => 'Stadt',
    'country' => 'Staat',
    'verify_email_header' => 'Bestätige deine Email-Adresse',
    'fresh_link_send_to_email' => 'Ein neuer Bestätigungslink wurde an Ihre E-Mail-Adresse gesendet.',
    'check_your_email_for_verification' => 'Bevor Sie fortfahren, überprüfen Sie bitte Ihre E-Mails auf einen Bestätigungslink.',
    'did_not_receive_email' => 'Wenn Sie die E-Mail nicht erhalten haben,',
    'click_to_request_another' => 'klicken Sie hier, um eine andere anzufordern.',
    'account_inactive' => 'Ihr Konto ist deaktiviert. Wenden Sie sich an Ihren Administrator.'
];