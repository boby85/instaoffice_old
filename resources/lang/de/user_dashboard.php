<?php

return [

	'no_new_messages' => 'Sie haben keine neuen Nachrichten.',
	'welcome_message_title' => 'Willkommen bei instaoffice.eu',
	'welcome_message_1' => 'Bitte benutzen Sie die Menü auf der linken Seite, um sich mit der Anwendung vertraut zu machen. Bei Fragen wenden Sie sich bitte an support@instaoffice.eu.',
	'welcome_message_2' => 'Wir wünschen Ihnen viel Spaß bei der Verwendung unserer Anwendung.'
];