<?php

return [

	'customer_number' => 'Kundennummer',
	'customers' => 'Kunden',
	'add_customer' => 'Kunde hinzufügen',
	'company_name' => 'Firmenname',
	'title' => 'Anrede',
	'mr' => 'Herr',
	'ms' => 'Frau',
	'name' => 'Name',
	'phone' => 'Telefon',
	'mobile_phone' => 'Mobiltelefon',
	'address' => 'Adresse',
	'city' => 'Stadt',
	'zip_code' => 'Postleitzahl',
	'country' => 'Staat',
	'email_address' => 'E-Mail-Addresse',
	'company_tax_number' => 'Steuernummer',
	'customer_details' => 'Kundendaten',
	'edit_customer' => 'Kunden bearbeiten',
	'customer_added' => 'Kunde erfolgreich hinzugefügt.',
	'customer_updated' => 'Kunde erfolgreich aktualisiert.',
	'customer_deleted' => 'Kunde erfolgreich gelöscht.',
	'no_customers' => 'Sie haben keine Kunden.',
	'customer_invoices' => 'Kundenrechnungen',
	'customer_offers' => 'Kundenangebote'
];