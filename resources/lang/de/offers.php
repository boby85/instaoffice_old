<?php

return [
	'offer' => 'Angebot',
	'offers' => 'Angebote',
	'add_offer' => 'Angebote hinzufügen',
	'edit_offer' => 'Angebot bearbeiten',
	'no_offers' => 'Sie haben keine Angebote.',
	'customer_no_offers' => 'Keine Angebote für diesen Kunden.',
	'no_customers' => 'Sie haben keine Kunden! Verwenden Sie das Menü links, um einen neuen Kunden hinzuzufügen.',
	'no_articles' => 'Sie haben keine Artikel! Verwenden Sie das Menü links, um einen neuen Artikel hinzuzufügen.',
	'offer_added' => 'Angebot erfolgreich erstellt.',
	'offer_updated' => 'Angebot erfolgreich aktualisiert.',
	'offer_deleted' => 'Angebot erfolgreich gelöscht.',
	'offer_number' => 'Angebotsnummer',
	'customer' => 'Kunde',
	'date' => 'Angebotsdatum',
	'expire_date' => 'Ablaufdatum',
	'article' => 'Artikel',
	'select_article' => 'Artikel auswählen',
	'price' => 'Preis',
	'quantity' => 'Menge',
	'discount' => 'Rabatt',
	'total' => 'Gesamt',
	'tax' => 'MwSt',
	'grand_total' => 'Gesamtsumme',
	'pdf_not_found' => 'PDF-Datei nicht gefunden! Klicken Sie auf Aktualisieren, um die Datei neu zu erstellen.',
	'tax_free_note' => 'Hinweis: Übergang der Ust-Schuld gemäß § 19 Abs. 1a UstG',
	'page' => 'Seite'
];