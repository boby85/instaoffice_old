<?php

return [
	'help' => 'Hilfe',
	'help_message' => 'Für anwendungsbezogene Hilfe kontaktieren Sie uns bitte unter support@instaoffice.eu.'
];