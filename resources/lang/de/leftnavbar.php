<?php

return [
    'dashboard' => 'Dashboard',
    'customers' => 'Kunden',
    'articles' => 'Artikel',
    'article_categories' => 'Artikelkategorien',
    'invoices' => 'Rechnungen',
    'settings' => 'Einstellungen',
    'user-settings' => 'Benutzereinstellungen',
    'invoices' => 'Rechnungen',
    'offers' => 'Angebote',
	'offers-invoices' => 'Angebote & Rechnungen',
    'help' => 'Hilfe',
    'users' => 'Benutzer',
    'push_messages' => 'Push-Mitteilungen'
];