<?php

return [

	'general_settings' => 'Allgemeine Einstellungen',
	'company_details' => 'Firma Daten:',
	'company_name' => 'Firma Name',
	'name' => 'Name',
	'first_name' => 'Vorname',
	'last_name' => 'Nachname',
	'address' => 'Adresse',
	'city' => 'Stadt',
	'zip' => 'Postleitzahl',
	'country' => 'Staat',
	'phone' => 'Telefon',
	'company_uid' => 'Firmen-UID',
	'company_tax_number' => 'Steuernummer',
	'company_register_number' => 'Firmenbuch',
	'company_logo' => 'Firmenlogo',
	'bank_details' => 'Bankdaten:',
	'other' => 'Andere:',
	'language' => 'Sprache',
	'settings_saved' => 'Einstellungen erfolgreich gespeichert.',

];