<?php

return [

	'back' => 'Zurück',
	'save' => 'Speichern',
	'update' => 'Aktualisieren',
	'cancel' => 'Benden',
	'delete' => 'Löschen',
	'add' => 'Hinzufügen',
	'manual_input' => 'Manuelle Eingabe',
	'preview' => 'Vorschau',
	'login' => 'Anmeldung',
	'register' => 'Registrieren',
	'send_password_reset_link' => 'Link zum Zurücksetzen des Passworts senden',
	'reset_password' => 'Passwort zurücksetzen',
	'confirm_password' => 'Passwort bestätigen',
	'deactivate_user' => 'Kunden deaktivieren',
	'activate_user' => 'Kunden aktivieren',
];