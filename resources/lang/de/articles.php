<?php

return [
	'articles' => 'Artikel',
	'add_article' => 'Artikel hinzufügen',
	'no_articles' => 'Sie haben keine Artikel.',
	'price' => 'Preis',
	'currency' => 'Währung',
	'article_details' => 'Artikeldaten',
	'description' => 'Beschreibung',
	'unit' => 'Einheit',
	'price_unit' => 'Preiseinheit',
	'piece' => 'Stück',
	'kg' => 'Kilogramm',
	'article_image' => 'Artikel Bild',
	'edit_article' => 'Artikel bearbeiten',
	'article_added' => 'Artikel erfolgreich hinzugefügt.',
	'article_updated' => 'Artikel erfolgreich aktualisiert.',
	'article_deleted' => 'Artikel erfolgreich gelöscht.',
];