<?php

return [
	'users' => 'Benutzer',
	'add_user' => 'Benutzer hinzufügen',
	'company_name' => 'Firmenname',
	'name' => 'Name',
	'phone' => 'Telefon',
	'address' => 'Adresse',
	'zip_code' => 'Postleitzahl',
	'city' => 'Stadt',
	'country' => 'Staat',
	'status' => 'Status',
	'role' => 'Rolle',
	'user_added' => 'Benutzer erfolgreich hinzugefügt.',
	'user_details' => 'Kundendetails',
	'edit_user' => 'Kunden bearbeiten',
	'company_name' => 'Firmenname',
	'user_updated' => 'Kunde erfolgreich aktualisiert.',
	'user_deleted' => 'Benutzer erfolgreich gelöscht.'
];