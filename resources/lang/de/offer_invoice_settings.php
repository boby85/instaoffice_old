<?php

return [
	'offer_invoice_settings' => 'Angebots- und Rechnungseinstellungen',
	'offer_settings' => 'Angebotseinstellungen',
	'offer_prefix' => 'Angebotsnummernpräfix',
	'offer_suffix' => 'Angebotsnummernsuffix',
	'offer_delimiter' => 'Angebotsnummernbegrenzer',
	'invoice_settings' => 'Rechnungseinstellungen',
	'invoice_prefix' => 'Rechnungsnummernpräfix',
	'invoice_suffix' => 'Rechnungsnummernsuffix',
	'invoice_delimiter' => 'Angebotsnummernbegrenzer',
	'example' => 'Beispiel',
	'settings_saved' => 'Einstellungen erfolgreich gespeichert.',
];