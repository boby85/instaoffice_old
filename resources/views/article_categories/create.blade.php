@extends('layouts.admin')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('article_categories.add_category') }}</h1>
    </div>
	<div class="container-fluid">
        <form method="POST" action="{{ route('article_categories.store') }}" enctype="multipart/form-data">
        @csrf
			<div class="row">
        		<div class="col-sm-3">
				    <div class="form-group">
				        <label for="category_name">{{ __('article_categories.category_name') }}</label> 
				        <input id="category_name" type="text" class="form-control @error('category_name') is-invalid @enderror" name="category_name" value="{{ old('category_name') }}" autofocus required="required">
				        @error('category_name')
				           	<span class="invalid-feedback" role="alert">
				            	<strong>{{ $message }}</strong>
				            </span>
				        @enderror
				    </div>
			    </div>
			</div>
			<div class="row">
        		<div class="col-sm-3">
				    <div class="form-group">
				        <label for="category_id">{{ __('article_categories.category_id') }}</label> 
				        <input id="category_id" type="text" class="form-control @error('category_id') is-invalid @enderror" name="category_id" value="{{ $next_id }}" autofocus required="required">
				        @error('category_id')
				           	<span class="invalid-feedback" role="alert">
				            	<strong>{{ $message }}</strong>
				            </span>
				        @enderror
				    </div>
			    </div>
			</div>
			<div class="row">
        		<div class="col-sm-3">
				    <div class="form-group">
				        <label for="category_parent_id">{{ __('article_categories.category_parent_id') }}</label> 
				        <input id="category_parent_id" type="text" class="form-control @error('category_parent_id') is-invalid @enderror" name="category_parent_id" value="{{ old('category_parent_id') }}" autofocus required="required">
				        @error('category_parent_id')
				           	<span class="invalid-feedback" role="alert">
				            	<strong>{{ $message }}</strong>
				            </span>
				        @enderror
				    </div>
			    </div>
			</div>
		    <div class="modal-footer">
				<button type="button" class="btn btn-secondary pull-left" onclick="window.location.href='{{ route('article_categories.index')}}'">{{ __('buttons.back') }}</button>
				<button type="submit" class="btn btn-primary">{{ __('buttons.save') }}</button>
			</div>
		</form>
	</div>
@endsection