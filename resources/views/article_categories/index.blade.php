@extends('layouts.admin')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('article_categories.article_categories') }}</h1>
    </div>
    <div class="">
        <button type="button" class="btn btn-primary text-nowrap" onclick="location.href='{{ route('article_categories.create') }}'" style="float:right; margin-bottom: 0.5em;">
            {{ __('article_categories.add_category') }}
        </button>
    </div>
    @if(!$article_categories->count())
        <p>{{ __('article_categories.no_article_categories') }}</p>
    @else
    <div class="table-responsive">
        <table id="article_categories-table" class="table">
            <thead>
                <tr>
                    <th>{{ __('article_categories.category_name') }}</th>
                    <th>{{ __('article_categories.category_id') }}</th>
                    <th>{{ __('article_categories.category_parent_id') }}</th>
                    <th></th>       
                </tr>
            </thead>
            <tbody>
                @foreach($article_categories as $category)
                <tr>
                    <td> {{ $category->category_name }}</td>
                    <td> {{ $category->category_id }} </td>
                    <td> {{ $category->category_parent_id }} </td>
                    <td>
                        <a href="{{ route('article_categories.edit', $category->id) }}">
                            <span class="feather-icons" data-feather="edit"></span>
                        </a>
                        <a href="#" data-toggle="modal" data-target="#confirm-delete-modal" data-url="{{ route('article_categories.destroy', $category->id) }}">
                            <span class="feather-icons" data-feather="trash-2"></span>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @endif
    @include('../common/confirm-delete-modal')
    <script>
        
        $('#confirm-delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            const url = button.data('url');
            $('#deleteFormClient').attr('action', url);
        });

        $(".alert.alert-success").fadeTo(4000, 500).slideUp(500, function(){
            $(".alert.alert-success").slideUp(500);
        });

    </script>
@endsection
