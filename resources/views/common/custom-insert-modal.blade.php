<div class="modal fade" id="custom-insert-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('messages.custom_insert') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form id="custom-insert-modal-form" action="" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name_modal">Name</label> 
                        <input id="name_modal" type="text" class="form-control" name="name_modal" maxlength="100" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="price_modal">{{ __('offers.price')}} €</label> 
                        <input id="price_modal" type="number" class="form-control" name="price_modal" step="0.01" min="0" value="0.00" required>
                    </div>
                    <div class="form-group">
                        <label for="quantity_modal">{{ __('offers.quantity')}}</label> 
                        <input id="quantity_modal" type="number" class="form-control" name="quantity_modal" step="1" min="1" max="10000" value="1" required>
                    </div>
                    <div class="form-group">
                        <label for="discount_modal">{{ __('offers.discount')}} %</label> 
                        <input id="discount_modal" type="number" class="form-control" name="discount_modal" step="0.05" min="0.00" max="100" value="0.00" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('buttons.cancel') }}</button> 
                    <button type="button" class="btn btn-primary" id="custom-insert-btn-add">{{ __('buttons.add') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>