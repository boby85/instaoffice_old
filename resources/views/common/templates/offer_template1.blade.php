<!doctype html>
<html lang="en">
    <head>
        <title>{{ __('offers.offer') }}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('css/offer-invoice.css') }}" >
    </head>
    <body>

	    <header id="header" class=""> <!-- header -->
	  		<div class="row header">
			    <div class="col-sm-auto logo">
			    	@if($user_settings->company_logo)
			     		<img src="{{ public_path() . '/storage/user_logo/' . $user_settings->company_logo }}" alt="Company Logo">
			     	@endif
			    </div>

			    <div class="col-sm-auto offer-user-details">
			    	<p>{{ $user_settings->company_name ?? $user_settings->name }}</p>
			    	<p>{{ ($user_settings->address . ', ' . $user_settings->zip_code . ' ' . $user_settings->city ) ?? '-' }}</p>
			    	<p>Tel: {{ $user_settings->phone ?? '-' }}</p>
			    	<p>E-mail: {{ $user_settings->email ?? '-' }}</p>
			    	@if($user_settings->website)
			    		<p>Web: {{ $user_settings->website ?? '-' }}</p>
			    	@endif
			    </div>
	  		</div>
	  		<hr class="header-hr">
	  		<div class="user-customer-details">
	  		<div class="row">
	  			<div class="col-sm-auto offer-customer-details">
	  				<b><p>{{ $offer->customer->company_name ?? $offer->customer->name }}</p></b>
	  				<p>{{ $offer->customer->address . ', ' }}</p>
	  				<p>{{ $offer->customer->city . ' ' . $offer->customer->country }}</p>
	  			</div>
	  			<div class="col-sm-auto offer-details">
	  				<p><b>{{ __('offers.offer_number') }}: {{ $offer->offer_number }}</b></p>
	  				<p>{{ __('offers.date') }}: {{ $offer->offer_date }}</p>
	  				<p>{{ __('offers.expire_date') }}: {{ $offer->offer_expire_date }}</p>
	  				<p>{{ __('offers.customer') }} Nr.: {{ $offer->customer->customer_number }}</p>
	  			</div>
	  		</div>
	  		</div> 	
	    </header><!-- /header -->

	    <footer> <!-- footer -->
	    	<hr class="footer-hr">
	    	<div class="row footer">
	    		<div class="col-sm-auto foo1">
	    			@if($user_settings->bank_name && $user_settings->bank_bic && $user_settings->bank_iban)
	    				<p>{{ $user_settings->bank_name }}</p>
	    				<p>{{ $user_settings->bank_iban }}</p>
	    				<p>{{ $user_settings->bank_bic }}</p>
	    			@endif	
	    		</div>
	    		<div class="col-sm-auto foo2">
	    			@if($user_settings->company_uid_number && $user_settings->company_tax_number && $user_settings->company_register_number)
		    			<p>{{ __('user_settings.company_uid') }}: {{ $user_settings->company_uid_number }}</p>
		    			<p>{{ __('user_settings.company_tax_number') }}: {{ $user_settings->company_tax_number }}</p>
		    			<p>{{ __('user_settings.company_register_number') }}: {{ $user_settings->company_register_number }}</p>
		    		@endif
	    		</div>
	    		<div class="col-sm-auto foo3">
	    			{{__('offers.page')}} <span class="pagenum"></span>
	    		</div>				
	    	</div>	
	    </footer><!-- /footer -->

	    <div style="page-break-after:auto;">

	    <div class="container content">
	    	<p class="offer_title">{{ __('offers.offer') }} {{ $offer->offer_number }}</p>
	    	<div class="row"> 
	  			@if($offer->articles)
	  				<table class="table offer-articles-table">
	  					<thead>
	  						<tr>
	  							<th>#</th>
	  							<th></th>
	  							<th>{{ __('offers.article') }}</th>
	  							<th>{{ __('offers.quantity') }}</th>
	  							<th>{{ __('offers.price') }}</th>
	  							<th>{{ __('offers.discount') }}</th>
	  							<th>{{ __('offers.total') }}</th>  								  								
	  						</tr>
	  					</thead>
	  					<tbody>
	  						@foreach($articles as $article)
	  							<tr>
	  								<td>
	  									{{ $loop->iteration }} 
	  								</td>
	  								<td>
	  									@if($article['image_name'] != 'no_image.png')
	  										<img src="{{ public_path() . '/storage/articles/' . $article['image_name'] }}" width="50px">
	  									@endif
	  								</td>
	  								<td>
	  									{{ $article['name'] }} 
	  								</td>
	  								<td>
	  									{{ $article['quantity'] }} 
	  								</td>
	  								<td>
	  									{{ $article['price'] }} 
	  								</td>  					
	  								<td>
	  									{{ $article['discount'] }} 
	  								</td>
	  								<td>
	  									{{ $article['total'] }} 
	  								</td>
	  							</tr>
	  						@endforeach
	  					</tbody>
	  					<tfoot>
	  						<tr>
	  							<td class="offer-amounts" colspan="7">
	  								<p>{{ __('offers.total') }}: {{ $offer->total }} &euro; </p>
	  								<p>{{ __('offers.tax') }} {{$offer->tax}}%: {{ $offer->tax_value  }} &euro; </p>
	  								<p>{{ __('offers.grand_total') }}: {{ $offer->grand_total }} &euro; </p>
	  							</td>
	  						</tr>
	  					</tfoot>
	  				</table>
	  			@endif
	  		</div>
	  		@if($offer->tax == 0 && $offer->total > 0)
	  		<div>
	  			{{ __('offers.tax_free_note') }}
	  		</div>
	  		@endif
	  	</div>
    </body>
</html>