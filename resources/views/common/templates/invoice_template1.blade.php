<!doctype html>
<html lang="en">
    <head>
        <title>{{ __('invoices.invoice') }}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('css/offer-invoice.css') }}" >
    </head>
    <body>

	    <header id="header" class=""> <!-- header -->
	  		<div class="row header">
			    <div class="col-sm logo">
			    	@if($user_settings->company_logo)
			     		<img src="{{ public_path() . '/storage/user_logo/' . $user_settings->company_logo }}" alt="Company Logo">
			     	@endif
			    </div>
	    		<div class="col-sm">
	    		</div>
			    <div class="col-sm invoice-user-details">
			    	<p>{{ $user_settings->company_name ?? $user_settings->name }}</p>
			    	<p>{{ ($user_settings->address . ', ' . $user_settings->zip_code . ' ' . $user_settings->city ) ?? '-' }}</p>
			    	<p>Tel: {{ $user_settings->phone ?? '-' }}</p>
			    	<p>E-mail: {{ $user_settings->email ?? '-' }}</p>
			    	<p>Web: {{ $user_settings->website ?? '-' }}</p>
			    </div>
	  		</div>
	  		<hr class="header-hr">
	  		<div class="row">
	  			<div class="col-sm invoice-customer-details">
	  				<b><p>{{ $invoice->customer->company_name ?? $invoice->customer->name }}</p></b>
	  				<p>{{ $invoice->customer->address . ', ' }}</p>
	  				<p>{{ $invoice->customer->city . ' ' . $invoice->customer->country }}</p>
	  				</div>
	  			<div class="col-sm invoice-details">
	  				<p><b>{{ __('invoices.invoice_number') }}: {{ $invoice->invoice_number }}</b></p>
	  				<p>{{ __('invoices.date') }}: {{ $invoice->invoice_date }}</p>
	  				<p>{{ __('invoices.customer') }} Nr.: 000{{ $invoice->customer_id }}</p>
	  			</div>
	  		</div> 	    	
	    </header><!-- /header -->

	    <footer> <!-- footer -->
	    	<hr class="footer-hr">
	    	<div class="row footer">
	    		<div class="col-sm foo1">
	    			@if($user_settings->bank_name && $user_settings->bank_bic && $user_settings->bank_iban)
	    				<p>{{ $user_settings->bank_name }}</p>
	    				<p>{{ $user_settings->bank_iban }}</p>
	    				<p>{{ $user_settings->bank_bic }}</p>
	    			@endif	
	    		</div>
	    		<div class="col-sm foo2">
	    			@if($user_settings->company_uid_number && $user_settings->company_tax_number && $user_settings->company_register_number)
		    			<p>{{ __('user_settings.company_uid') }}: {{ $user_settings->company_uid_number }}</p>
		    			<p>{{ __('user_settings.company_tax_number') }}: {{ $user_settings->company_tax_number }}</p>
		    			<p>{{ __('user_settings.company_register_number') }}: {{ $user_settings->company_register_number }}</p>
		    		@endif
	    		</div>
	    		<div class="col-sm foo3">
	    			{{__('invoices.page')}} <span class="pagenum"></span>
	    		</div>				
	    	</div>	
	    </footer><!-- /footer -->

	    <div style="page-break-after:auto;">

	    <div class="container content">
	    	<p class="invoice_title">{{ __('invoices.invoice') }} {{ $invoice->invoice_number }}</p>
	    	<div class="row"> 
	  			@if($invoice->articles)
	  				<table class="table invoice-articles-table">
	  					<thead>
	  						<tr>
	  							<th>#</th>
	  							<th>{{ __('invoices.article') }}</th>
	  							<th>{{ __('invoices.quantity') }}</th>
	  							<th>{{ __('invoices.price') }}</th>
	  							<th>{{ __('invoices.discount') }}</th>
	  							<th>{{ __('invoices.total') }}</th>  								  								
	  						</tr>
	  					</thead>
	  					<tbody>
	  						@foreach($articles as $article)
	  							<tr>
	  								<td>
	  									{{ $loop->iteration }} 
	  								</td>
	  								<td>
	  									{{ $article['name'] }} 
	  								</td>
	  								<td>
	  									{{ $article['quantity'] }} 
	  								</td>
	  								<td>
	  									{{ $article['price'] }} 
	  								</td>  					
	  								<td>
	  									{{ $article['discount'] }} 
	  								</td>
	  								<td>
	  									{{ $article['total'] }} 
	  								</td>
	  							</tr>
	  						@endforeach
	  					</tbody>
	  					<tfoot>
	  						<tr>
	  							<td class="invoice-amounts" colspan="7">
	  								<p>{{ __('invoices.total') }}: {{ $invoice->total }} &euro; </p>
	  								<p>{{ __('invoices.tax') }} {{$invoice->tax}}%: {{ $invoice->tax_value  }} &euro; </p>
	  								<p>{{ __('invoices.grand_total') }}: {{ $invoice->grand_total }} &euro; </p>
	  							</td>
	  						</tr>
	  					</tfoot>
	  				</table>
	  			@endif
	  		</div>
	  		@if($invoice->tax == 0 && $invoice->total > 0)
	  		<div>
	  			{{ __('invoices.tax_free_note') }}
	  		</div>
	  		@endif
	  	</div>
    </body>
</html>