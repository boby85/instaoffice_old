<div class="modal fade" id="confirm-delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('messages.confirm_delete') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form id="deleteFormClient" method="POST">
                @method('DELETE')
                @csrf 
                <div class="modal-body">
                    <p>{{ __('messages.sure_to_delete') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('buttons.cancel') }}</button>                    
                    <button type="submit" class="btn btn-danger btn-delete">{{ __('buttons.delete') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>