<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('messages.edit_article') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form id="edit-modal-form" action="" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="edit_name">Name</label> 
                        <input id="edit_name" type="text" class="form-control" name="edit_name" disabled>
                    </div>
                    <div class="form-group">
                        <label for="edit_price">{{ __('invoices.price')}} €</label> 
                        <input id="edit_price" type="number" class="form-control" name="edit_price" step="0.01" min="0" value="" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="edit_quantity">{{ __('invoices.quantity')}}</label> 
                        <input id="edit_quantity" type="number" class="form-control" name="edit_quantity" step="1" min="1" max="10000" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="edit_discount">{{ __('invoices.discount')}} %</label> 
                        <input id="edit_discount" type="number" class="form-control" name="edit_discount" step="0.05" min="0.00" max="100" value="" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('buttons.cancel') }}</button> 
                    <button type="button" class="btn btn-primary" id="editBtnModal">{{ __('buttons.update') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>