@extends('layouts.user')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('leftnavbar.dashboard') }}</h1>
    </div>
    <div class="dashboard_messages">
    	@if($show_welcome_message)
    		<div class="dashboard_message welcome-message">
                <div>
                    <span id="close" class="message-close">x</span>
                    <p class="title">{{ __('user_dashboard.welcome_message_title') }}</p>
    			    <p class="text">
                        {{ __('user_dashboard.welcome_message_1') }}
                        <br/>
                        {{ __('user_dashboard.welcome_message_2') }}
                        <br/>
                        <br/>
                        Instaoffice team.
                    </p>
                </div>
    		</div>
        @endif
        <div class="no-messages">
    		<p> {{ __('user_dashboard.no_new_messages') }} </p>
        </div>
    </div>

    <script>
		$(document).ready(function(){
			showHideNoMessages();
			$(".message-close").click(function(){
		    	$(this).parents("div").eq(1).remove();
		    	showHideNoMessages();
		  	});
		});

		function showHideNoMessages()
		{
			var itemsNum = $('.dashboard_message').length;
			if (itemsNum == 0)
				$('.no-messages').show();
			if (itemsNum > 0)
				$('.no-messages').hide();
		}

</script>
@endsection
