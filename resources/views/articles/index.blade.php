@extends('layouts.user')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('articles.articles') }}</h1>
    </div>
    
    <table id="articles-table" class="display responsive" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>{{ __('articles.price_unit') }}</th>
                <th>{{ __('articles.unit') }}</th>
                <th>{{ __('articles.price') }}</th>
                <th>{{ __('articles.currency') }}</th> 
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    
    <script>
        $(document).ready( function () {
            $('#articles-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                responsive: {
                    details: false
                },
                columnDefs: [
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 1, targets: 1 }
                ],
                ajax: "{{ route('articles.index') }}",
                language:{
                    url: ($('html')[0].lang == 'de') ? '/js/localization/dataTables_de.json' : ''
                },
                columns: [
                    {data: 'image', name: 'image', orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'price_unit', name: 'price_unit', class:'not-mobile'},
                    {data: 'unit', name: 'unit', class:'not-mobile'},
                    {data: 'price', name: 'price', class:'not-mobile'},
                    {data: 'currency', name: 'currency', class:'not-mobile'},
                ]
            });
            $('#articles-table').on( 'click', 'tr', function () {
                var articleTable = $('#articles-table').DataTable();
                var id = articleTable.row( this ).data().id;
                window.location = 'articles/' + id;
            });  
        });
    </script>
@endsection
