@extends('layouts.user')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('articles.article_details') }}</h1>
    </div>
	
	<div class="row">
        <div class="article-image col-sm-auto">
        	<img src="/{{ $image_path ?? '' }}" class="" alt="article image">
        </div>
    	<div class="article-code col-sm">
			<h5> 
                <span data-feather="chevrons-right"></span> 
                {{ $article->code }} 
            </h5>
            <h5> 
                <span data-feather="chevrons-right"></span> 
                {{ $article->main_product_group }} 
            </h5>
			<h5> 
                <span data-feather="chevrons-right"></span> 
                {{ $article->name }} 
            </h5>
			<h5>
                <span data-feather="chevrons-right"></span>
                    {{ $article->price }} {{ $article->currency }} ({{ $article->price_unit }}/{{$article->unit}})
            </h5>
            <h5> 
                <span data-feather="chevrons-right"></span> 
                {{ $article->net_weight }} {{ $article->weight_unit }}
            </h5>
    	</div>
    </div>
    <div class="row">
    	<div class="article-description col-sm">
			<span data-feather="chevrons-right"></span>
                {{ $article->description ?? '-' }}
    	</div>
	</div>

    <button type="button" class="btn btn-secondary show-back" onclick="window.location.href='{{ route('articles.index')}}'">
        {{ __('buttons.back') }}
    </button>

@endsection