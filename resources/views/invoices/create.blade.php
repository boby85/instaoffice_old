@extends('layouts.user')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('invoices.add_invoice')}}</h1>
    </div>
    <div class="container-fluid">
        <form id="invoice-form" action="{{ route('invoices.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="invoice_number">{{ __('invoices.invoice_number')}}</label> 
                        <input type="text" class="form-control @error('invoice_number') is-invalid @enderror" name="invoice_number" value="{{ $invoice_number }}" autofocus>
                        @error('invoice_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="customer">{{ __('invoices.customer')}}</label> 
                        <select name="customer_id" class="form-control @error('customer_id') is-invalid @enderror">
                            <option value=""> {{ __('invoices.select_customer')}} </option>
                            @foreach($customers as $customer)   
                                <option type="text" name="customer"
                                    value="{{ $customer->id }}">{{ $customer->company_name ?? $customer->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('customer_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="invoice_date">{{ __('invoices.date')}}</label> 
                        <input id="invoice_date" type="date" class="form-control @error('invoice_date') is-invalid @enderror" name="invoice_date" value="{{ date('Y-m-d') }}" autofocus>
                        @error('invoice_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <input id="articleslist" type="hidden" name="articleslist">
            <input id="tax-free-invoice" type="hidden" name="tax-free-invoice">
        </form>
        <form id="articles-fields">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group form-invoice-inline-elements">
                        <label for="article">{{ __('invoices.article')}}</label>
                        <select id="article_select" name="article_select" class="form-control">
                            <option></option>
                            @foreach($articles as $article)
                                <option class="form-control" name="article" value="{{ $article->id }}" title="{{ $article->price }}">
                                    {{ $article->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group form-invoice-inline-elements">
                        <label for="price">{{ __('invoices.price')}} €</label> 
                        <input id="price" type="number" class="form-control" name="price" step="0.01" min="0" value="0.00" autofocus>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group form form-invoice-inline-elements">
                        <label for="quantity">{{ __('invoices.quantity')}}</label> 
                        <input id="quantity" type="number" class="form-control" name="quantity" step="1" min="1" max="10000" value="1" autofocus>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group form-invoice-inline-elements">
                        <label for="discount">{{ __('invoices.discount')}} %</label> 
                        <input id="discount" type="number" class="form-control" name="discount" step="0.05" min="0.00" max="100" value="0.00" autofocus>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group form-invoice-inline-elements">
                        <button type="button" class="btn btn-primary add-article">{{ __('buttons.add')}}</button>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group form-invoice-inline-elements">
                        <button type="button" class="btn btn-primary custom-insert" data-toggle="modal" data-target="#custom-insert-modal">
                            {{ __('buttons.manual_input')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
        @include('../common/custom-insert-modal')
        @include('../common/edit-modal')
        <div class="row invoice-article-image-preview">
            <div class="col-sm-auto">
                <img id="article-image-preview" src="" width="100px" height="auto">
            </div>
            <div class="col-sm-auto">
                <p id="product_group"></p>
                <p id="price_unit"></p>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table" id="invoice-articles-table" name="articles">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('invoices.article')}}</th>
                        <th>{{ __('invoices.price')}} €</th>
                        <th>{{ __('invoices.quantity')}}</th>
                        <th>{{ __('invoices.discount')}} %</th>
                        <th>{{ __('invoices.total')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6"></td>
                        <td>
                            <span>{{ __('invoices.total')}}</span>
                            <span id="total">0.00</span>
                            <span>€</span>
                        </td>
                    </tr>
                    <tr class="invoice-tax-total">
                        <td colspan="6"></td>
                        <td>
                            <span>{{ __('invoices.tax')}}</span>
                            <span class="tax-20">20%</span>
                            <span id="tax">0.00</span>
                            <span>€</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"></td>
                        <td>
                            <span>{{ __('invoices.grand_total')}}</span>
                            <span id="grandTotal">0.00</span>
                            <span>€</span>            
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="invoice-buttons">
            <div class="row">
                <div class="col">
                    <input type="checkbox" id="tax-free" name="tax-free"> {{ __('invoices.tax_free_note') }}
                </div>
                <div class="col-auto">
                    <button type="button" class="btn btn-secondary" onclick="window.location.href='{{ route('invoices.index')}}'">
                        {{ __('buttons.back')}}
                    </button>
                    <button id="submit-invoice-button" type="submit" class="btn btn-success btn-save">
                        {{ __('buttons.save')}}
                    </button>
                </div> 
            </div>
        </div>
    </div>
<script src="{{ asset('js/invoiceHelpers.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#article_select').select2({
            placeholder: "{{ __('invoices.select_article') }}",
            theme: "bootstrap4"
        }).on('change', function(){
            var optionPrice = $('option:selected', this).attr('title');
            $('#price').val(optionPrice);
        });
    });
</script>

@endsection