@extends('layouts.user')

@section('content')
    <div class="mb-3 border-bottom">
        <h1 class="h2">{{ __('invoices.invoices') }}</h1>
    </div>
    <div class="">
        <button type="button" class="btn btn-primary text-nowrap" onclick="location.href='{{ route('invoices.create') }}'" style="float:right; margin-bottom: 0.5em;">
            {{ __('invoices.add_invoice') }}
        </button>
    </div>
    @if(!$invoices->count())
        <p>{{ __('invoices.no_invoices') }}</p>
    @else
        <table id="invoices-table" class="display responsive nowrap" width="100%">
            <thead>
                <tr>
                    <th>{{ __('invoices.invoice_number') }}</th>
                    <th>{{ __('invoices.customer') }}</th>
                    <th>{{ __('invoices.date') }}</th>
                    <th>{{ __('invoices.total') }}</th>
                    <th>{{ __('invoices.discount') }}</th>
                    <th>{{ __('invoices.grand_total') }}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($invoices as $invoice)
            <tr>
                <td> {{ $invoice->invoice_number }} </td>
                @if($invoice->customer)
                    <td> {{ $invoice->customer->company_name ?? $invoice->customer->name }} </td>
                @else
                    <td> - </td>
                @endif
                <td> {{ $invoice->invoice_date }} </td>
                <td> {{ $invoice->total }} </td>
                <td> {{ $invoice->discount }} </td>
                <td> {{ $invoice->grand_total }} </td>
                <td> 
                    <a href="{{ route('invoices.show', $invoice->id) }}" target="_blank">
                        <span class="feather-icons" data-feather="eye"></span>
                    </a>
                    @if($invoice->customer)
                        <a href="{{ route('invoices.edit', $invoice->id) }}">
                            <span class="feather-icons" data-feather="edit"></span>
                        </a>
                    @endif
                    <a href="#" data-toggle="modal" data-target="#confirm-delete-modal" data-url="{{ route('invoices.destroy', $invoice->id) }}">
                        <span class="feather-icons" data-feather="trash-2"></span>
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @include('../common/confirm-delete-modal')
    <script>
        $(document).ready( function () {
            var input = {
                stateSave: true,
                responsive: {
                    details: false
                },
                columnDefs: [
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 1, targets: 6 }
                ]
            }; 
            if($('html')[0].lang == 'de') {
                input.language ={url: '/js/localization/dataTables_de.json'};
            }
            $('#invoices-table').DataTable(input);
        });

        $('#confirm-delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            const url = button.data('url');
            $('#deleteFormClient').attr('action', url);
        });

        $(".alert.alert-success").fadeTo(4000, 500).slideUp(500, function(){
            $(".alert.alert-success").slideUp(500);
        });
    </script>

@endsection
