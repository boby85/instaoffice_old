@extends('layouts.user')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('offers.edit_offer')}}</h1>
    </div>
    <div class="container-fluid">
        <form id="offer-form" action="{{ route('offers.update', $offer->id) }}" method="POST" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="offer_number">{{ __('offers.offer_number')}}</label> 
                        <input type="text" class="form-control @error('offer_number') is-invalid @enderror" name="offer_number" value="{{ $offer->offer_number }}" autofocus>
                        @error('offer_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="customer">{{ __('offers.customer')}}</label> 
                        <select name="customer_id" class="form-control @error('customer_id') is-invalid @enderror">
                            @foreach($customers as $customer)   
                                <option type="text" name="customer" value="{{ $customer->id }}" @if($customer->id == $offer->customer_id) selected @endif>
                                    {{ $customer->company_name ?? $customer->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('customer_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="offer_date">{{ __('offers.date')}}</label> 
                        <input id="offer_date" type="date" class="form-control @error('offer_date') is-invalid @enderror" name="offer_date" value="{{ $offer->offer_date }}" autofocus>
                        @error('offer_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="offer_expire_date">{{ __('offers.expire_date')}}</label> 
                        <input id="offer_expire_date" type="date" class="form-control @error('offer_expire_date') is-invalid @enderror" name="offer_expire_date" value="{{ $offer->offer_expire_date }}" autofocus>
                        @error('offer_expire_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <input id="articleslist" type="hidden" name="articleslist">
            <input id="tax-free-offer" type="hidden" name="tax-free-offer">
        </form>
        <form id="articles-fields">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group form-offer-inline-elements">
                        <label for="article">{{ __('offers.article')}}</label>
                        <select id="article_select" name="article_select" class="form-control">
                            <option value="0"></option>
                            @foreach($articles as $article)
                                <option class="form-control" name="article" value="{{ $article->id }}" title="{{ $article->price }}">
                                    {{ $article->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group form-offer-inline-elements">
                        <label for="price">{{ __('offers.price')}} €</label> 
                        <input id="price" type="number" class="form-control" name="price" step="0.01" min="0" value="0.00" autofocus>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group form form-offer-inline-elements">
                        <label for="quantity">{{ __('offers.quantity')}}</label> 
                        <input id="quantity" type="number" class="form-control" name="quantity" step="1" min="1" max="10000" value="1" autofocus>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group form-offer-inline-elements">
                        <label for="discount">{{ __('offers.discount')}} %</label> 
                        <input id="discount" type="number" class="form-control" name="discount" step="0.05" min="0.00" max="100" value="0.00" autofocus>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group form-offer-inline-elements">
                        <button type="button" class="btn btn-primary add-article">{{ __('buttons.add')}}</button>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group form-offer-inline-elements">
                        <button type="button" class="btn btn-primary custom-insert" data-toggle="modal" data-target="#custom-insert-modal">
                            {{ __('buttons.manual_input')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
        @include('../common/custom-insert-modal')
        @include('../common/edit-modal')
        <div class="row offer-article-image-preview">
            <div class="col-sm-auto">
                <img id="article-image-preview" src="" width="100px" height="auto">
            </div>
            <div class="col-sm-auto">
                <p id="product_group"></p>
                <p id="price_unit"></p>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table" id="offer-articles-table" name="articles">
                <thead>
                    <tr>
                    	<th>#</th>
                        <th>{{ __('offers.article')}}</th>
                        <th>{{ __('offers.price')}} €</th>
                        <th>{{ __('offers.quantity')}}</th>
                        <th>{{ __('offers.discount')}} %</th>
                        <th>{{ __('offers.total')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($offer_articles as $offer_article)
                        <tr>
                        	<input type="hidden" name="article_id" class="tdId" value="{{ $offer_article->article_id }}">
                        	<td class="tdImage">
                        		<img id="offer-article-image" src="{{ '../../storage/articles/' . $offer_article->image_name }}" width="50px" height="auto">
                        	</td>
                            <td class="tdArticle">{{ $offer_article->name }}</td>
                            <td class="tdPrice">{{ $offer_article->price }}</td>
                            <td class="tdQuantity">{{ $offer_article->quantity }}</td>
                            <td class="tdDiscount">{{ $offer_article->discount }}</td>
                            <td class="tdPriceTotal">{{ $offer_article->total }}</td>
                            <td>
		                        <button type="button" class="btn btnEdit" data-toggle="modal" data-target="#edit-modal">
		                            <span class="feather-icons" data-feather="edit"></span>
		                        </button>
		                        <button type="button" class="btn btnDelete">
		                            <span class="feather-icons" data-feather="trash-2"></span>
		                        </button>   
                            </td>
                        </tr>
                    @endforeach 
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6"></td>
                        <td>
                            <span>{{ __('offers.total')}}</span>
                            <span id="total">{{$offer->total}}</span>
                            <span>€</span>
                        </td>
                    </tr>
                    <tr class="offer-tax-total">
                        <td colspan="6"></td>
                        <td>
                            <span>{{ __('offers.tax')}}</span>
                            <span class="tax-20">{{ ($offer->tax == 0) ? '0%' : '20%' }}</span>
                            <span id="tax">{{$offer->tax_value}}</span>
                            <span>€</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"></td>
                        <td>
                            <span>{{ __('offers.grand_total')}}</span>
                            <span id="grandTotal">{{$offer->grand_total}}</span>
                            <span>€</span>                
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="offer-buttons">
            <div class="row">
                <div class="col">
                    <input type="checkbox" id="tax-free" name="tax-free" @if($offer->tax == 0) checked @endif> 
                    {{ __('offers.tax_free_note') }}
                </div>
                <div class="col-auto">
                    <button type="button" class="btn btn-secondary" onclick="window.history.back()">
                        {{ __('buttons.back')}}
                    </button>
                    <button id="submit-offer-button" type="submit" class="btn btn-success btn-save">
                        {{ __('buttons.update')}}
                    </button>
                </div> 
            </div>
        </div>
    </div>
<script src="{{ asset('js/offerHelpers.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#article_select').select2({
            placeholder: "{{ __('offers.select_article') }}",
            theme: "bootstrap4"
        }).on('change', function(){
            var optionPrice = $('option:selected', this).attr('title');
            $('#price').val(optionPrice);
        });
    });
</script>

@endsection