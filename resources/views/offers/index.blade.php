@extends('layouts.user')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('offers.offers') }}</h1>
    </div>
    <div class="">
        <button type="button" class="btn btn-primary text-nowrap" onclick="location.href='{{ route('offers.create') }}'" style="float:right; margin-bottom: 0.5em;">
            {{ __('offers.add_offer') }}
        </button>
    </div>
    @if(!$offers->count())
        <p>{{ __('offers.no_offers') }}</p>
    @else
        <table id="offers-table" class="display responsive nowrap" width="100%">
            <thead>
                <tr>
                    <th>{{ __('offers.offer_number') }}</th>
                    <th>{{ __('offers.customer') }}</th>
                    <th>{{ __('offers.date') }}</th>
                    <th>{{ __('offers.expire_date') }}</th>
                    <th>{{ __('offers.total') }}</th>
                    <th>{{ __('offers.discount') }}</th>
                    <th>{{ __('offers.grand_total') }}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($offers as $offer)
            <tr>
                <td> {{ $offer->offer_number }} </td>
                @if($offer->customer)
                    <td> {{ $offer->customer->company_name ?? $offer->customer->name }} </td>
                @else
                    <td> - </td>
                @endif
                <td> {{ $offer->offer_date }} </td>
                <td> {{ $offer->offer_expire_date }} </td>
                <td> {{ $offer->total }} </td>
                <td> {{ $offer->discount }} </td>
                <td> {{ $offer->grand_total }} </td>
                <td> 
                    <a href="{{ route('offers.show', $offer->id) }}" target="_blank">
                        <span class="feather-icons" data-feather="eye"></span>
                    </a>
                    @if($offer->customer)
                        <a href="{{ route('offers.edit', $offer->id) }}">
                            <span class="feather-icons" data-feather="edit"></span>
                        </a>
                    @endif
                    <a href="#" data-toggle="modal" data-target="#confirm-delete-modal" data-url="{{ route('offers.destroy', $offer->id) }}">
                        <span class="feather-icons" data-feather="trash-2"></span>
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    @endif
    @include('../common/confirm-delete-modal')
    <script>
        $(document).ready( function () {
            var input = {
                stateSave: true,
                responsive: {
                    details: false
                },
                columnDefs: [
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 1, targets: 7 }
                ]
            }; 
            if($('html')[0].lang == 'de') {
                input.language ={url: '/js/localization/dataTables_de.json'};
            }
            $('#offers-table').DataTable(input);
        });

        $('#confirm-delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            const url = button.data('url');
            $('#deleteFormClient').attr('action', url);
        });

        $(".alert.alert-success").fadeTo(4000, 500).slideUp(500, function(){
            $(".alert.alert-success").slideUp(500);
        });
    </script>

@endsection
