@extends('layouts.user')

@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
		<h1 class="h2">{{ __('user_settings.general_settings') }}</h1>
	</div>
	<div class="container-fluid">
		<form method="POST" action="/user-settings/{{ $user_settings->id }}" enctype="multipart/form-data">
        @method('PATCH')
		@csrf
		<div class="container-fluid border settings settings-company-details">
			<p>{{ __('user_settings.company_details') }}</p>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="company_name" class="col-form-label col-form-label-sm">{{ __('user_settings.company_name') }}</label> 
						<input id="company_name" type="text" class="form-control form-control-sm @error('company_name') is-invalid @enderror" name="company_name" value="{{ $user_settings->company_name ?? '' }}" autofocus>
						@error('company_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="name" class="col-form-label col-form-label-sm">{{ __('user_settings.name') }}</label> 
						<input id="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" value="{{ $user_settings->name ?? '' }}">
						@error('name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>              	
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="address" class="col-form-label col-form-label-sm">{{ __('user_settings.address') }}</label> 
						<input id="address" type="text" class="form-control form-control-sm @error('address') is-invalid @enderror" name="address" value="{{ $user_settings->address ?? '' }}">
						@error('address')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="city" class="col-form-label col-form-label-sm">{{ __('user_settings.city') }}</label> 
						<input id="city" type="text" class="form-control form-control-sm @error('city') is-invalid @enderror" name="city" value="{{ $user_settings->city ?? '' }}">
						@error('city')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<label for="zip_code" class="col-form-label col-form-label-sm">{{ __('user_settings.zip') }}</label> 
						<input id="zip_code" type="text" class="form-control form-control-sm @error('zip_code') is-invalid @enderror" name="zip_code" value="{{ $user_settings->zip_code ?? '' }}">
						@error('zip_code')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="country" class="col-form-label col-form-label-sm">{{ __('user_settings.country') }}</label> 
						<input id="country" type="text" class="form-control form-control-sm @error('country') is-invalid @enderror" name="country" value="{{ $user_settings->country ?? '' }}">
						@error('country')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="phone" class="col-form-label col-form-label-sm">{{ __('user_settings.phone') }}</label> 
						<input id="phone" type="text" class="form-control form-control-sm @error('phone') is-invalid @enderror" name="phone" value="{{ $user_settings->phone ?? '' }}">
						@error('phone')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="email" class="col-form-label col-form-label-sm">Email</label> 
						<input id="email" type="text" class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" value="{{ $user_settings->email ?? '' }}" disabled="disabled">
						@error('email')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="website" class="col-form-label col-form-label-sm">Web</label> 
						<input id="website" type="text" class="form-control form-control-sm @error('website') is-invalid @enderror" name="website" value="{{ $user_settings->website ?? '' }}">
						@error('website')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="company_uid_number" class="col-form-label col-form-label-sm">{{ __('user_settings.company_uid') }}</label> 
						<input id="company_uid_number" type="text" class="form-control form-control-sm @error('company_uid_number') is-invalid @enderror" name="company_uid_number" value="{{ $user_settings->company_uid_number ?? '' }}">
						@error('company_uid_number')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="company_tax_number" class="col-form-label col-form-label-sm">{{ __('user_settings.company_tax_number') }}</label> 
						<input id="company_tax_number" type="text" class="form-control form-control-sm @error('company_tax_number') is-invalid @enderror" name="company_tax_number" value="{{ $user_settings->company_tax_number ?? '' }}">
						@error('company_tax_number')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="company_register_number" class="col-form-label col-form-label-sm">{{ __('user_settings.company_register_number') }}</label> 
						<input id="company_register_number" type="text" class="form-control form-control-sm @error('company_register_number') is-invalid @enderror" name="company_register_number" value="{{ $user_settings->company_register_number ?? '' }}">
						@error('company_register_number')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>                	
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="company_logo" class="col-form-label col-form-label-sm">{{ __('user_settings.company_logo') }}</label> 
						<input id="company_logo" type="file" class="form-control form-control-sm @error('company_logo') is-invalid @enderror" name="company_logo">
						@error('company_logo')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<img src="/{{ $logo_path ?? '' }}" class="" alt="company logo">
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid border settings settings-bank-data">
			<p>{{ __('user_settings.bank_details') }}</p>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="bank_name" class="col-form-label col-form-label-sm">Bank name</label> 
						<input id="bank_name" type="text" class="form-control form-control-sm @error('bank_name') is-invalid @enderror" name="bank_name" value="{{ $user_settings->bank_name ?? '' }}">
						@error('bank_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="bank_iban" class="col-form-label col-form-label-sm">IBAN</label> 
						<input id="bank_iban" type="text" class="form-control form-control-sm @error('bank_iban') is-invalid @enderror" name="bank_iban" value="{{ $user_settings->bank_iban ?? '' }}">
						@error('bank_iban')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="bank_bic" class="col-form-label col-form-label-sm">BIC</label> 
						<input id="bank_bic" type="text" class="form-control form-control-sm @error('bank_bic') is-invalid @enderror" name="bank_bic" value="{{ $user_settings->bank_bic ?? '' }}">
						@error('bank_bic')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>               
			</div>
		</div>
		<div class="container-fluid border settings settings-other">
			<p>{{ __('user_settings.other') }}</p>
			<div class="row">
				<div class="col-md-1">
					<div class="form-group">
						<label for="language" class="col-form-label col-form-label-sm">{{ __('user_settings.language') }}</label>
						<select name="language" class="form-control form-control-sm @error('language') is-invalid @enderror"> 
							<option type="text" name="language" value="en" @if($user_settings->language == 'en') selected @endif>EN</option>
							<option type="text" name="language" value="de" @if($user_settings->language == 'de') selected @endif>DE</option>
						</select>
						@error('language')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
            <button type="submit" class="btn btn-primary">{{ __('buttons.update') }}</button>
        </div>
		</form>
	</div>
	<script>
        $(".alert.alert-success").fadeTo(4000, 500).slideUp(500, function(){
            $(".alert.alert-success").slideUp(500);
        });
    </script>
@endsection
                            