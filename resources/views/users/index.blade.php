@extends('layouts.admin')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('users.users') }}</h1>
    </div>
    @if(!$users->count())
        <p>{{ __('users.no_users') }}</p>
    @else
    <div class="table-responsive">
        <table id="users-table" class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>{{ __('users.name') }}</th>
                    <th>{{ __('users.company_name') }}</th>
                    <th>{{ __('users.phone') }}</th>
                    <th>E-mail</th>
                    <th>{{ __('users.status') }}</th>
                    <th>{{ __('users.role') }}</th>
                    <th></th>       
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
                <td> {{ $user->id }} </td>  
                <td> {{ $user->name }} </td>
                <td> {{ $user->company_name ?? '-' }} </td>
                <td> {{ $user->phone }} </td>
                <td> {{ $user->email }} </td>
                <td class="{{ ($user->active) ? 'user-active' : 'user-inactive' }}"> {{ ($user->active) ? 'ACTIVE' : 'INACTIVE' }}</td>

                <td> {{ $user->role->role_name }}</td>
                <td> 
                    <a href="{{ route('users.show', $user->id) }}">
                        <span class="feather-icons" data-feather="eye"></span>
                    </a>
                    @if($user->id != Auth::user()->id)
                    <a href="#" data-toggle="modal" data-target="#confirm-delete-modal" data-url="{{ route('users.destroy', $user->id) }}">
                        <span class="feather-icons" data-feather="trash-2"></span>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @endif
    @include('../common/confirm-delete-modal')
    <script>
        
        $('#confirm-delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            const url = button.data('url');
            $('#deleteFormClient').attr('action', url);
        });

        $(".alert.alert-success").fadeTo(4000, 500).slideUp(500, function(){
            $(".alert.alert-success").slideUp(500);
        });

    </script>
@endsection
