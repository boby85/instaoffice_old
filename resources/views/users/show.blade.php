@extends('layouts.admin')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('users.user_details') }}</h1>
    </div>
	<div class="container-fluid user-details">
        <div class="user first-last-name">
			<span class="feather-icons" data-feather="user"></span> {{ $user->first_name }} {{ $user->last_name }} [{{ ($user->active) ? 'ACTIVE' : 'INACTIVE' }}]
        </div>
        <div class="user company-name">
            <span class="feather-icons" data-feather="trello"></span> {{ ($user->user_settings->company_name) ? $user->user_settings->company_name : '-' }}
        </div>
        <div class="user address">
			<span class="feather-icons" data-feather="map-pin"></span> {{ $user->user_settings->address }}, {{ $user->user_settings->zip_code }} {{ $user->user_settings->city }} {{ $user->user_settings->country }}
        </div>
        <div class="user phone">
			<span class="feather-icons" data-feather="phone"></span> {{ $user->user_settings->phone }}
        </div>
        <div class="user email">
			<span class="feather-icons" data-feather="mail"></span> {{ $user->user_settings->email }}
        </div>
    </div>
    
    <hr/>

    <button type="button" class="btn btn-secondary show-back" onclick="window.location.href='{{ route('users.index')}}'">
        {{ __('buttons.back') }}
    </button>

    <form method="POST" action="/users/{{ $user->id }}" enctype="multipart/form-data">
    @method('PATCH')
    @csrf

        @if($user->active)
            <input type="hidden" name="action" value="deactivate">
            <button type="button" class="btn btn-danger show-back" onclick="this.form.submit()">
                {{ __('buttons.deactivate_user') }}
            </button>
        @else
            <input type="hidden" name="action" value="activate">
            <button type="button" class="btn btn-primary show-back" onclick="this.form.submit()">
                {{ __('buttons.activate_user') }}
            </button>
        @endif
        
    </form>
@endsection