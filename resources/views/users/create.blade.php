@extends('layouts.admin')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('users.add_user') }}</h1>
    </div>
	<div class="container-fluid">
        <form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
        @csrf

			<div class="row">
    			<div class="col-md-2">
			        <div class="form-group">
			            <label for="first_name">{{ __('users.first_name') }}</label> 
			            <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name"value="{{ old('first_name') }}" autofocus required>
			                @error('first_name')
			                	<span class="invalid-feedback" role="alert">
			                    	<strong>{{ $message }}</strong>
			                    </span>
			                @enderror
			        </div>
			    </div>
    			<div class="col-md-2">
			        <div class="form-group">
			            <label for="last_name">{{ __('users.last_name') }}</label> 
			            <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name"value="{{ old('last_name') }}" autofocus required>
			                @error('last_name')
			                	<span class="invalid-feedback" role="alert">
			                    	<strong>{{ $message }}</strong>
			                    </span>
			                @enderror
			        </div>
			    </div>
			</div>

			<div class="row">
    			<div class="col-md-2">
			        <div class="form-group">
			            <label for="company_name">{{ __('users.company_name') }}</label> 
			            <input id="company_name" type="text" class="form-control @error('company_name') is-invalid @enderror" name="company_name"value="{{ old('company_name') }}" autofocus>
			                @error('company_name')
			                	<span class="invalid-feedback" role="alert">
			                    	<strong>{{ $message }}</strong>
			                    </span>
			                @enderror
			        </div>
			    </div>
			</div>

			<div class="row">
    			<div class="col-md-2">
			        <div class="form-group">
			            <label for="email">E-mail</label> 
			            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email"value="{{ old('email') }}" autofocus required>
			                @error('email')
			                	<span class="invalid-feedback" role="alert">
			                    	<strong>{{ $message }}</strong>
			                    </span>
			                @enderror
			        </div>
			    </div>
			</div>

			<div class="row">
	    		<div class="col-md-2">
			        <div class="form-group">
			            <label for="phone">{{ __('users.phone') }}</label> 
			            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone"value="{{ old('phone') }}" autofocus required>
				            @error('phone')
				            	<span class="invalid-feedback" role="alert">
				                	<strong>{{ $message }}</strong>
				                </span>
				            @enderror
				    </div>
				</div>
			</div>

			<div class="row">
	    		<div class="col-md-2">
			        <div class="form-group">
			            <label for="address">{{ __('users.address') }}</label> 
			            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address"value="{{ old('address') }}" autofocus required>
				            @error('address')
				            	<span class="invalid-feedback" role="alert">
				                	<strong>{{ $message }}</strong>
				                </span>
				            @enderror
				    </div>
				</div>
	    		<div class="col-md-1">
			        <div class="form-group">
			            <label for="zip_code">{{ __('users.zip_code') }}</label> 
			            <input id="zip_code" type="text" class="form-control @error('zip_code') is-invalid @enderror" name="zip_code"value="{{ old('zip_code') }}" autofocus required>
				            @error('zip_code')
				            	<span class="invalid-feedback" role="alert">
				                	<strong>{{ $message }}</strong>
				                </span>
				            @enderror
				    </div>
				</div>
	    		<div class="col-md-2">
			        <div class="form-group">
			            <label for="city">{{ __('users.city') }}</label> 
			            <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city"value="{{ old('city') }}" autofocus required>
				            @error('city')
				            	<span class="invalid-feedback" role="alert">
				                	<strong>{{ $message }}</strong>
				                </span>
				            @enderror
				    </div>
				</div>				
			</div>

			<div class="row">
	    		<div class="col-md-2">
			        <div class="form-group">
			            <label for="country">{{ __('users.country') }}</label> 
			            <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country"value="{{ old('country') }}" autofocus required>
				            @error('country')
				            	<span class="invalid-feedback" role="alert">
				                	<strong>{{ $message }}</strong>
				                </span>
				            @enderror
				    </div>
				</div>
			</div>

			<div class="row">
	    		<div class="col-md-2">
			        <div class="form-group">
			            <label for="role_name">{{ __('users.role') }}</label> 
			            <select id="role_name" type="text" class="form-control @error('role_name') is-invalid @enderror" name="role_name">
			            	<option value="user">User</option>
			            	<option value="admin">Admin</option>
			            </select>
				            @error('role_name')
				            	<span class="invalid-feedback" role="alert">
				                	<strong>{{ $message }}</strong>
				                </span>
				            @enderror
				    </div>
				</div>
			</div>

	        <div class="modal-footer">
	            <button type="button" class="btn btn-secondary pull-left" onclick="window.location.href='{{ route('users.index')}}'">{{ __('buttons.back') }}</button>
	            <button type="submit" class="btn btn-primary">{{ __('buttons.save') }}</button>
	        </div>
   		</form>
	</div>
@endsection