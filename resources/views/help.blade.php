@extends('layouts.user')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('help.help') }}</h1>
    </div>
    <div class="help_messages">
    	<div class="help_message contact-message">
            <p> {{ __('help.help_message') }}</p>
    	</div>
    </div>
@endsection
