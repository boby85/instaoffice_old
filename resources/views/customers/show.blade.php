@extends('layouts.user')

@section('content')
    <div class="mb-3 border-bottom">
        <h4>{{ __('customers.customer_details') }}</h4>
    </div>
	<div class="container-fluid customer-details">
        <div class="row">
            <div class="col">
                <div class="customer customer-number">
                    <span class="feather-icons" data-feather="hash"></span> {{ $customer->customer_number }}
                </div>
                @if($customer->company_name)
                 	<div class="customer company-name">
            			<span class="feather-icons" data-feather="trello"></span> {{ $customer->company_name ?? '-' }}
                    </div>
                @endif
                <div class="customer name">
                    <span class="feather-icons" data-feather="user"></span>
                    {{ __('customers.' . $customer->title) }} {{ $customer->name }}
                </div>
                <div class="customer created-at">
                    <span class="feather-icons" data-feather="calendar"></span> {{ $customer->created_at }}
                </div>
                @if($customer->company_tax_number)
                    <div class="customer company-tax-number">
                        <span class="feather-icons" data-feather="zap"></span> {{ $customer->company_tax_number ?? '-' }}
                    </div>
                @endif
                <div class="customer address">
        			<span class="feather-icons" data-feather="map-pin"></span> {{ $customer->address }}, {{ $customer->zip_code }} {{ $customer->city }}, {{ $customer->country }}
                </div>
            </div>
            <div class="col-8">
                <div class="customer phone">
        			<span class="feather-icons" data-feather="phone"></span> {{ $customer->phone }}
                </div>
                @if($customer->mobile_phone)
                    <div class="customer mobile-phone">
                        <span class="feather-icons" data-feather="smartphone"></span> {{ $customer->mobile_phone ?? '-' }}
                    </div>
                @endif
                @if($customer->fax)
                    <div class="customer fax">
            			<span class="feather-icons" data-feather="phone-forwarded"></span> {{ $customer->fax ?? '-' }}
                    </div>
                @endif
                <div class="customer email">
        			<span class="feather-icons" data-feather="mail"></span> {{ $customer->email }}
                </div>
                @if($customer->website)
                    <div class="customer website">
            			<span class="feather-icons" data-feather="globe"></span> {{ $customer->website ?? '-' }}
                    </div>
                @endif
            </div>
        </div>
    </div>
    
    <hr/>

    @if($offers->count())
        <h4>{{ __('customers.customer_offers') }}</h4>

            <table id="offers-table" class="display responsive nowrap" width="100%">
                <thead>
                    <tr>
                        <th>{{ __('offers.offer_number') }}</th>
                        <th>{{ __('offers.customer') }}</th>
                        <th>{{ __('offers.date') }}</th>
                        <th>{{ __('offers.expire_date') }}</th>
                        <th>{{ __('offers.total') }}</th>
                        <th>{{ __('offers.discount') }}</th>
                        <th>{{ __('offers.grand_total') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($offers as $offer)
                <tr>
                    <td> {{ $offer->offer_number }} </td>
                    @if($offer->customer)
                        <td> {{ $offer->customer->company_name ?? $offer->customer->name }} </td>
                    @else
                        <td> - </td>
                    @endif
                    <td> {{ $offer->offer_date }} </td>
                    <td> {{ $offer->offer_expire_date }} </td>
                    <td> {{ $offer->total }} </td>
                    <td> {{ $offer->discount }} </td>
                    <td> {{ $offer->grand_total }} </td>
                    <td> 
                        <a href="{{ route('offers.show', $offer->id) }}" target="_blank">
                            <span class="feather-icons" data-feather="eye"></span>
                        </a>
                        @if($offer->customer)
                            <a href="{{ route('offers.edit', $offer->id) }}">
                                <span class="feather-icons" data-feather="edit"></span>
                            </a>
                        @endif
                        <a href="#" data-toggle="modal" data-target="#confirm-delete-modal" data-url="{{ route('offers.destroy', $offer->id) }}">
                            <span class="feather-icons" data-feather="trash-2"></span>
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        <hr/>
    @endif

    @if($invoices->count())
        <h4>{{ __('customers.customer_invoices') }}</h4>

            <table id="invoices-table" class="display responsive nowrap" width="100%">
                <thead>
                    <tr>
                        <th>{{ __('invoices.invoice_number') }}</th>
                        <th>{{ __('invoices.customer') }}</th>
                        <th>{{ __('invoices.date') }}</th>
                        <th>{{ __('invoices.total') }}</th>
                        <th>{{ __('invoices.discount') }}</th>
                        <th>{{ __('invoices.grand_total') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($invoices as $invoice)
                <tr>
                    <td> {{ $invoice->invoice_number }} </td>
                    @if($invoice->customer)
                        <td> {{ $invoice->customer->company_name ?? $invoice->customer->name }} </td>
                    @else
                        <td> - </td>
                    @endif
                    <td> {{ $invoice->invoice_date }} </td>
                    <td> {{ $invoice->total }} </td>
                    <td> {{ $invoice->discount }} </td>
                    <td> {{ $invoice->grand_total }} </td>
                    <td> 
                        <a href="{{ route('invoices.show', $invoice->id) }}" target="_blank">
                            <span class="feather-icons" data-feather="eye"></span>
                        </a>
                        @if($invoice->customer)
                            <a href="{{ route('invoices.edit', $invoice->id) }}">
                                <span class="feather-icons" data-feather="edit"></span>
                            </a>
                        @endif
                        <a href="#" data-toggle="modal" data-target="#confirm-delete-modal" data-url="{{ route('invoices.destroy', $invoice->id) }}">
                            <span class="feather-icons" data-feather="trash-2"></span>
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        <hr/>
    @endif

    <button type="button" class="btn btn-secondary show-back" onclick="window.location.href='{{ route('customers.index')}}'">
        {{ __('buttons.back') }}
    </button>

    @include('../common/confirm-delete-modal')
    <script>
        $(document).ready( function () {
            var inputOffers = {
                stateSave: true,
                responsive: {
                    details: false
                },
                columnDefs: [
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 1, targets: 7 }
                ],
            };
            var inputInvoices = {
                stateSave: true,
                responsive: {
                    details: false
                },
                columnDefs: [
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 1, targets: 6 }
                ],
            }; 
            if($('html')[0].lang == 'de') {
                inputOffers.language = {url: '/js/localization/dataTables_de.json'};
                inputInvoices.language = {url: '/js/localization/dataTables_de.json'};
            }
            $('#offers-table').DataTable(inputOffers);
            $('#invoices-table').DataTable(inputInvoices);
        });

        $('#confirm-delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            const url = button.data('url');
            $('#deleteFormClient').attr('action', url);
        });

        $(".alert.alert-success").fadeTo(4000, 500).slideUp(500, function(){
            $(".alert.alert-success").slideUp(500);
        });
    </script> 

@endsection