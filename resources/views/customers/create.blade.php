@extends('layouts.user')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('customers.add_customer') }}</h1>
    </div>
	<div class="container-fluid">
        <form method="POST" action="{{ route('customers.store') }}" enctype="multipart/form-data">
        @csrf
			<div class="row">

        		<div class="col-md-3 add-customer-left"> <!-- LEFT SIDE -->
            		<div class="row">
                		<div class="col-sm-6">
				            <div class="form-group">
				                <label for="customer_number">{{ __('customers.customer_number') }}</label> 
				                <input id="customer_number" type="text" class="form-control @error('customer_number') is-invalid @enderror" name="customer_number" value="{{ $customer_number }}" autofocus required="required">
				                @error('customer_number')
				                   	<span class="invalid-feedback" role="alert">
				                    	<strong>{{ $message }}</strong>
				                    </span>
				                @enderror
				            </div>
			            </div>
		                <div class="col-sm-12">
			                <div class="form-group">
			                    <label for="company_name">{{ __('customers.company_name') }}</label> 
			                    <input id="company_name" type="text" class="form-control @error('company_name') is-invalid @enderror" name="company_name" value="{{ old('company_name') }}" autofocus>
			                    @error('company_name')
			                    	<span class="invalid-feedback" role="alert">
			                        	<strong>{{ $message }}</strong>
			                        </span>
			                    @enderror
			                </div>
		                </div>
		                <div class="col-sm-12">
		                    <div class="row">
		                        <div class="col-sm-4">
					                <div class="form-group">
					                	<label for="title">{{ __('customers.title') }}</label> 
					                    <select id="title" class="form-control @error('title') is-invalid @enderror" name="title">
				                            <option type="text" name="title" value="Mr"> {{ __('customers.mr') }} </option>
				                        	<option type="text" name="title" value="Ms"> {{ __('customers.ms') }} </option>
				                        </select>
					                    @error('title')
					                    	<span class="invalid-feedback" role="alert">
					                    	    <strong>{{ $message }}</strong>
					                    	</span>
					                    @enderror
					                </div>
		                        </div>
		                        <div class="col-sm-8">
					                <div class="form-group">
					                	<label for="name">{{ __('customers.name') }}</label> 
					                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autofocus required="required">
					                    @error('name')
					                    	<span class="invalid-feedback" role="alert">
					                    	    <strong>{{ $message }}</strong>
					                    	</span>
					                    @enderror
					                </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="col-sm-12">
					        <div class="form-group">
					            <label for="address">{{ __('customers.address') }}</label> 
					            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" autofocus required="required">
					            @error('address')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
		                </div>
		                <div class="col-sm-12">
		                    <div class="row">
		                        <div class="col-sm-4">
						            <div class="form-group">
						                <label for="zip_code">{{ __('customers.zip_code') }}</label> 
						                <input id="zip_code" type="text" class="form-control @error('zip_code') is-invalid @enderror" name="zip_code" value="{{ old('zip_code') }}" autofocus required="required">
						                @error('zip_code')
						                    <span class="invalid-feedback" role="alert">
						                        <strong>{{ $message }}</strong>
						                    </span>
						                @enderror
						            </div>
		                        </div>
		                        <div class="col-sm-8">
					                <div class="form-group">
					                    <label for="city">{{ __('customers.city') }}</label> 
					                    <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" autofocus required="required">
					                    @error('city')
					                        <span class="invalid-feedback" role="alert">
					                            <strong>{{ $message }}</strong>
					                        </span>
					                    @enderror
					                </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="col-sm-12">
					        <div class="form-group">
					            <label for="country">{{ __('customers.country') }}</label> 
					            <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('country') }}" autofocus required="required">
					            @error('country')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
		                </div>
		                <div class="col-sm-12">
					        <div class="form-group">
					            <label for="company_tax_number">{{ __('customers.company_tax_number') }}</label> 
					            <input id="company_tax_number" type="text" class="form-control @error('company_tax_number') is-invalid @enderror" name="company_tax_number" value="{{ old('company_tax_number') }}" autofocus>
					            @error('company_tax_number')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					        </div>
		                </div>		                

		            </div>    
            	</div> <!-- END LEFT SIDE -->

        		<div class="col-md-3 add-customer-right"> <!-- RIGHT SIDE -->
            		<div class="row">
                		<div class="col-sm-12">
			        		<div class="form-group">
					            <label for="phone">{{ __('customers.phone') }}</label> 
					            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone"value="{{ old('phone') }}" autofocus required="required">
					            @error('phone')
					            	<span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					       	</div>
                		</div>
		                <div class="col-sm-12">
			        		<div class="form-group">
					            <label for="mobile_phone">{{ __('customers.mobile_phone') }}</label> 
					            <input id="mobile_phone" type="text" class="form-control @error('mobile_phone') is-invalid @enderror" name="mobile_phone"value="{{ old('mobile_phone') }}" autofocus>
					            @error('mobile_phone')
					            	<span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					       	</div>
		                </div>
		                <div class="col-sm-12">
			        		<div class="form-group">
					            <label for="fax">Fax</label> 
					            <input id="fax" type="text" class="form-control @error('fax') is-invalid @enderror" name="fax"value="{{ old('fax') }}" autofocus>
					            @error('fax')
					            	<span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					       	</div>
		                </div>
		                <div class="col-sm-12">
			        		<div class="form-group">
					            <label for="email">{{ __('customers.email_address') }}</label> 
					            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"value="{{ old('email') }}" autofocus required="required">
					            @error('email')
					            	<span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					       	</div>
		                </div>
		                <div class="col-sm-12">
			        		<div class="form-group">
					            <label for="website">Web</label> 
					            <input id="website" type="text" class="form-control @error('website') is-invalid @enderror" name="website"value="{{ old('website') }}" autofocus>
					            @error('website')
					            	<span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					            @enderror
					       	</div>
		                </div>
            		</div>
        		</div> <!-- END RIGHT SIDE -->

   		 	</div>
		    <div class="modal-footer">
				<button type="button" class="btn btn-secondary pull-left" onclick="window.location.href='{{ route('customers.index')}}'">{{ __('buttons.back') }}</button>
				<button type="submit" class="btn btn-primary">{{ __('buttons.save') }}</button>
			</div>  
   		</form>
    </div>
@endsection