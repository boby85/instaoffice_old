@extends('layouts.user')

@section('content')
    <div class="mb-3 border-bottom">
        <h1 class="h2">{{ __('customers.customers') }}</h1>
    </div>
    <div>
        <button type="button" class="btn btn-primary text-nowrap" onclick="location.href='{{ route('customers.create') }}'" style="float:right; margin-bottom: 0.5em;">
            {{ __('customers.add_customer') }}
        </button>
    </div>
    @if(!$customers->count())
        <p>{{ __('customers.no_customers') }}</p>
    @else
        <table id="customers-table" class="display responsive nowrap" width="100%">
            <thead>
                <tr>
                    <th>{{ __('customers.customer_number') }}</th>
                    <th>{{ __('customers.company_name') }}</th>
                    <th>{{ __('customers.name') }}</th>
                    <th>{{ __('customers.phone') }}</th>
                    <th>E-mail</th>
                    <th></th>       
                </tr>
            </thead>
            <tbody>
                @foreach($customers as $customer)
                <tr>
                    <td> {{ $customer->customer_number ?? '-' }} </td>
                    <td> {{ $customer->company_name ?? '-' }} </td>
                    <td> {{ $customer->name }} </td>
                    <td> {{ $customer->phone }} </td>
                    <td> {{ $customer->email }} </td>
                    <td> 
                        <a href="{{ route('customers.show', $customer->id) }}">
                            <span class="feather-icons" data-feather="eye"></span>
                        </a>
                        <a href="{{ route('customers.edit', $customer->id) }}">
                            <span class="feather-icons" data-feather="edit"></span>
                        </a>
                        <a href="#" data-toggle="modal" data-target="#confirm-delete-modal" data-url="{{ route('customers.destroy', $customer->id) }}">
                            <span class="feather-icons" data-feather="trash-2"></span>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif
    @include('../common/confirm-delete-modal')
    <script>
        $(document).ready( function () {
            var input = {
                stateSave: true,
                responsive: {
                    details: false
                },
                columnDefs: [
                    { responsivePriority: 1, targets: 2 },
                    { responsivePriority: 1, targets: 5 }
                ]
            }; 
            if($('html')[0].lang == 'de') {
                input.language = {url: '/js/localization/dataTables_de.json'};
            }
            $('#customers-table').DataTable(input);
        });

        $('#confirm-delete-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            const url = button.data('url');
            $('#deleteFormClient').attr('action', url);
        });

        $(".alert.alert-success").fadeTo(4000, 500).slideUp(500, function(){
            $(".alert.alert-success").slideUp(500);
        });
    </script>
@endsection