@extends('layouts.user')

@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
		<h1 class="h2">{{ __('offer_invoice_settings.offer_invoice_settings') }}</h1>
	</div>
	<div class="container-fluid">
		<form method="POST" action="offer-invoice-settings/{{ Auth::user()->id }}" enctype="multipart/form-data">
	    @method('PATCH')
		@csrf
		<div class="row">
			<div class="col">
					<div class="container-fluid border settings offer-settings">
						<p>{{ __('offer_invoice_settings.offer_settings') }}</p>
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="offer_prefix" class="col-form-label col-form-label-sm">{{ __('offer_invoice_settings.offer_prefix') }}</label>
									<input id="offer_prefix" type="text" class="form-control form-control-sm @error('offer_prefix') is-invalid @enderror" name="offer_prefix" value="{{ $offer_invoice_settings->offer_prefix ?? '' }}" autofocus required>
									@error('offer_prefix')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="offer_suffix" class="col-form-label col-form-label-sm">{{ __('offer_invoice_settings.offer_suffix') }}</label> 
									<input id="offer_suffix" type="text" class="form-control form-control-sm @error('offer_suffix') is-invalid @enderror" name="offer_suffix" value="{{ $offer_invoice_settings->offer_suffix ?? '' }}" required>
									@error('offer_suffix')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">               	
							<div class="col-md-2">
								<div class="form-group">
									<label for="offer_delimiter" class="col-form-label col-form-label-sm">{{ __('offer_invoice_settings.offer_delimiter') }}</label> 
									<input id="offer_delimiter" type="text" class="form-control form-control-sm @error('offer_delimiter') is-invalid @enderror" name="offer_delimiter" value="{{ $offer_invoice_settings->offer_delimiter ?? '' }}" required>
									@error('offer_delimiter')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="container-fluid border settings invoice-settings">
						<p>{{ __('offer_invoice_settings.invoice_settings') }}</p>
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="invoice_prefix" class="col-form-label col-form-label-sm">{{ __('offer_invoice_settings.invoice_prefix') }}</label>
									<input id="invoice_prefix" type="text" class="form-control form-control-sm @error('invoice_prefix') is-invalid @enderror" name="invoice_prefix" value="{{ $offer_invoice_settings->invoice_prefix ?? '' }}" required>
									@error('invoice_prefix')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="invoice_suffix" class="col-form-label col-form-label-sm">{{ __('offer_invoice_settings.invoice_suffix') }}</label> 
									<input id="invoice_suffix" type="text" class="form-control form-control-sm @error('invoice_suffix') is-invalid @enderror" name="invoice_suffix" value="{{ $offer_invoice_settings->invoice_suffix ?? '' }}" required>
									@error('invoice_suffix')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">              	
							<div class="col-md-2">
								<div class="form-group">
									<label for="invoice_delimiter" class="col-form-label col-form-label-sm">{{ __('offer_invoice_settings.invoice_delimiter') }}</label> 
									<input id="invoice_delimiter" type="text" class="form-control form-control-sm @error('invoice_delimiter') is-invalid @enderror" name="invoice_delimiter" value="{{ $offer_invoice_settings->invoice_delimiter ?? '' }}" required>
									@error('invoice_delimiter')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>                	
						</div>
					</div>	
			    </div>
			</div>
			<div class="modal-footer">
			    <button type="submit" class="btn btn-primary">{{ __('buttons.update') }}</button>
			</div>
		</form>
	</div>
	<script>
        $(".alert.alert-success").fadeTo(4000, 500).slideUp(500, function(){
            $(".alert.alert-success").slideUp(500);
        });
    </script>
@endsection
                            