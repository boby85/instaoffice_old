<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'InstaOffice') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- <script src="{{ asset('js/jquery.dataTables.js') }}"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script> -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"> </script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    @if(app()->getLocale() == 'de')
        <script src="{{ asset('js/localization/jQueryValidation_de.js') }}"> </script>
    @endif
    <script>
    	$(window).on('load', function(){
		    $('.loading').fadeOut();
		})
    </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('css/jquery.dataTables.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2-bootstrap4-min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/overrides.css') }}" rel="stylesheet">

    
</head>
<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>InstaOffice</h3>
            </div>
            <ul class="list-unstyled components">
                <li class="nav-item {{ (request()->routeIs('user_dashboard*')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('user_dashboard') }}">
                        <span data-feather="home"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.dashboard') }} </span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('customers*')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('customers.index') }}">
                        <span data-feather="users"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.customers') }} </span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('articles*')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('articles.index') }}">
                        <span data-feather="layers"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.articles') }} </span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('offers*')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('offers.index') }}">
                        <span data-feather="file-plus"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.offers') }} </span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('invoices*')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('invoices.index') }}">
                        <span data-feather="file-text"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.invoices') }} </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span data-feather="settings"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.settings') }} </span>
                    </a>
                    <ul class="collapse list-unstyled {{ (request()->routeIs('user-settings.index') || request()->routeIs('offer-invoice-settings.index')) ? 'show' : '' }}" id="pageSubmenu">
                        <li class="nav-item {{ (request()->routeIs('user-settings.index')) ? 'active' : '' }}">
                            <a href="{{ route('user-settings.index')}}">
                                {{ __('leftnavbar.user-settings') }}
                            </a>
                        </li>
                        <li class="nav-item {{ (request()->routeIs('offer-invoice-settings.index')) ? 'active' : '' }}">
                            <a href="{{ route('offer-invoice-settings.index')}}">
                                {{ __('leftnavbar.offers-invoices') }} 
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ (request()->routeIs('help*')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('help') }}">
                        <span data-feather="help-circle"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.help') }} </span>
                    </a>
                </li>
            </ul>
        </nav>

        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container-fluid">
                    <button type="button" id="sidebarCollapse" class="btn btn-info d-lg-none">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown user-dropdown">
                                <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('password.request') }}">
                                        <span class="feather-icons" data-feather="refresh-cw"></span> {{ __('topbar.change_password') }}
                                    </a>
                                    <a class="dropdown-item user-logout" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <span class="feather-icons" data-feather="log-out"></span> {{ __('topbar.signout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
           	<main role="main" class="main-content">
                <div class="loading"></div>
            	@include('flash-message')
                @yield('content')
        	</main>
    	</div>
    </div>
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
        feather.replace() //Left nav icons
    </script>
</body>
</html>
