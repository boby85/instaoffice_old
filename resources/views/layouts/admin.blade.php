<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'InstaOffice') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/overrides.css') }}" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

</head>
<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>InstaOffice</h3>
            </div>
            <ul class="list-unstyled components">
                <li class="nav-item {{ (request()->routeIs('users*')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('users.index') }}">
                        <span data-feather="users"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.users') }} </span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('article_categories*')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('article_categories.index') }}">
                        <span data-feather="list"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.article_categories') }} </span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('push_messages*')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('push_messages.index') }}">
                        <span data-feather="message-square"></span>
                        <span class="sidebar-item"> {{ __('leftnavbar.push_messages') }} </span>
                    </a>
                </li>
            </ul>
        </nav>

        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container-fluid">
                    <button type="button" id="sidebarCollapse" class="btn btn-info d-md-none">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item text-nowrap">
                                <a class="nav-link" href="{{ route('logout') }}" 
                                    onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                    {{Auth::user()->email}} [{{ __('topbar.signout') }}]
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
           	<main role="main" class="main-content">
            	@include('flash-message')
                @yield('content')
        	</main>
    	</div>
    </div>
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
        feather.replace() //Left nav icons
    </script>
</body>
</html>
