<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
    	'user_id', 'customer_number', 'company_name', 'title', 'name', 'address', 'zip_code', 'city', 
        'country', 'company_tax_number', 'phone', 'mobile_phone', 'fax', 'email', 'website'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    
    public function offers()
    {
        return $this->hasMany('App\Offer');
    }

    public function invoices()
    {
    	return $this->hasMany('App\Invoice');
    }
}
