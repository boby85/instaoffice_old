<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
    	'user_id', 'offer_number', 'customer_id', 'offer_date', 'offer_expire_date',
        'total', 'discount', 'tax', 'tax_value', 'grand_total', 'pdf_file'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function customer()
    {
    	return $this->belongsTo('App\Customer');
    }

    public function invoice()
    {
        return $this->hasOne('App\Invoice');
    }

    public function articles()
    {
    	return $this->hasMany('App\OfferArticle');
    }
}
