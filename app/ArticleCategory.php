<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    use HasFactory;

    protected $fillable = [
    	'category_name', 'category_id', 'category_parent_id'
    ];

    public function article()
    {
    	return $this->hasMany('App\Article');
    }
}
