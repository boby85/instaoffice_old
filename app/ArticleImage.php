<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleImage extends Model
{
    use HasFactory;

    protected $fillable = [
    	'article_id', 'name'
    ];

    public function article()
	{
		return $this->belongsTo('App\Article');
	}
}
