<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceArticle extends Model
{
    use HasFactory;

    protected $fillable = [
    	'invoice_id', 'article_id', 'image_path', 'name', 'quantity', 'price', 'total'
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }
}
