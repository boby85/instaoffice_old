<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    protected $fillable = [
    	'user_id', 'name', 'company_name', 'company_logo', 'address', 'city', 'zip_code', 'country', 'phone', 
    	'email', 'website', 'company_uid_number', 'company_tax_number', 'company_register_number', 'bank_name', 'bank_bic', 'bank_iban' 
    ];

    public function user()
    {
    	return belongsTo('App\User');
    }
}
