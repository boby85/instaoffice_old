<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
    	'user_id', 'invoice_number', 'customer_id', 'invoice_date',
        'total', 'discount', 'tax', 'tax_value', 'grand_total', 'pdf_file'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function customer()
    {
    	return $this->belongsTo('App\Customer');
    }

    public function offer()
    {
        return $this->hasOne('App\Offer');
    }

    public function articles()
    {
    	return $this->hasMany('App\InvoiceArticle');
    }
}
