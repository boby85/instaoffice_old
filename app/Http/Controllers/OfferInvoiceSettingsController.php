<?php

namespace App\Http\Controllers;

use App\OfferInvoiceSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class OfferInvoiceSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
        $offer_invoice_settings = $user->offer_invoice_settings;
        return view('offer_invoice_settings.index', compact('offer_invoice_settings'));
    }

    public function update(Request $request, OfferInvoiceSettings $offerInvoiceSettings, $id)
    {
        $attributes = $this->validateOfferInvoiceSettings();
        $attributes['user_id'] = Auth::user()->id;
        OfferInvoiceSettings::updateOrCreate(
        	['user_id' => $attributes['user_id']], 
        	$attributes
        );

        return redirect ('/offer-invoice-settings')->with('success', __('offer_invoice_settings.settings_saved'));
    }

    protected function validateOfferInvoiceSettings()
    {
        return request()->validate([
            'offer_prefix' => ['required','alpha','min:2','max:2'],
            'offer_suffix' => ['required','digits:4'],
            'offer_delimiter' => ['required','in:/,-,#'],
            'invoice_prefix' => ['required','alpha','min:2','max:2'],
            'invoice_suffix' => ['required','digits:4'],
            'invoice_delimiter' => ['required','in:/,-,#']
        ]);
    }

}
