<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserDashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $show_welcome_message = (!Auth::user()->last_login) ? true : false;
    	Auth::user()->last_login = date("Y-m-d h:m:s");
    	Auth::user()->save();

        return view('user_dashboard', compact('show_welcome_message'));
    }

}
