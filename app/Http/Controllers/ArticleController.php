<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleImage;
use Illuminate\Http\Request;
use DataTables;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {   
        if ($request->ajax()) {
            $data = Article::with('ArticleImage');
            return Datatables::of($data)
                    ->addColumn('image', function($row){
                            $path = ($row->ArticleImage) ? 'storage/articles/' . $row->ArticleImage->name : 'storage/articles/fallback.png';
                            $img = '<img src="'. $path .'" alt="article image" width="50px" height="auto">';
                        return $img;
                    })
                    ->rawColumns(['action', 'image'])
                    ->make(true);
        } 
         return view ('articles.index');
    }
    
    public function show(Article $article)
    {
        $image_path = ($article->articleImage) ? 
            'storage/articles/' . $article->articleImage->name : 
            'storage/articles/fallback.png';
       
        return view('articles.show', compact('article','image_path')); 
    }
    
}
