<?php

namespace App\Http\Controllers;

use App\PushMessage;
use Illuminate\Http\Request;

class PushMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('push_messages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PushMessage  $pushMessage
     * @return \Illuminate\Http\Response
     */
    public function show(PushMessage $pushMessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PushMessage  $pushMessage
     * @return \Illuminate\Http\Response
     */
    public function edit(PushMessage $pushMessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PushMessage  $pushMessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PushMessage $pushMessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PushMessage  $pushMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(PushMessage $pushMessage)
    {
        //
    }
}
