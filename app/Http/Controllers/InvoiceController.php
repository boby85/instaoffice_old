<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\User;
use App\Article;
use App\InvoiceArticle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Arr;

class InvoiceController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
        $invoices = $user->invoices;

        return view('invoices.index', compact('invoices'));
    }


    public function create()
    {
        $user = User::findOrFail(Auth::user()->id);
        $customers = $user->customers;
        if(!$customers->count())
            return back()->with('error', __('offers.no_customers'));
        $invoice_number = $this->generateInvoiceNumber($user);
        $articles = Article::all();

        return view('invoices.create', compact('customers', 'invoice_number', 'articles'));
    }

    public function store(Request $request)
    {
    	$user = User::findOrFail(Auth::user()->id);
        $attributes = $this->validateInvoice($current_invoice_number = null, $user);
        $attributes['user_id'] = $user->id;
        $attributes['total'] = 0;
        $attributes['discount'] = 0;
        (request('tax-free-invoice') === 'true') ? 
            $attributes['tax'] = 0 : $attributes['tax'] = 20;
        $attributes['pdf_file'] = 'Invoice_' . time() . '.pdf';

        //Calculate sum for invoice
        $articles = json_decode($attributes['articleslist'], true);
        foreach ($articles as $article) {
            $attributes['total'] += $article['total'];
        }
        $attributes['total'] = number_format((float)$attributes['total'], 2, '.', '');
        $attributes['tax_value'] = ($attributes['total'] * ($attributes['tax']/100));
        $attributes['tax_value'] = number_format((float)$attributes['tax_value'], 2, '.', '');
        $attributes['grand_total'] = $attributes['total'] + $attributes['tax_value'];
        $attributes['grand_total'] = number_format((float)$attributes['grand_total'], 2, '.', '');

        //Create invoice
        $invoice = Invoice::create($attributes);

        //Add fields for articles which are not created automatically
        $articles = collect($articles)->map(function ($item) use($invoice) {
            $item['invoice_id'] = $invoice->id;
            $item['created_at'] = now()->toDateString();
            $item['updated_at'] = now()->toDateString();
            return $item;
        });

        //Insert articles
        InvoiceArticle::insert($articles->toArray());

        //Generate PDF
        $user_settings = $user->user_settings;
        $pdf = PDF::loadView('common.templates.invoice_template1', compact('invoice', 'articles', 'user_settings'));
        //return $pdf->stream();
        $pdf->save(storage_path() .'/app/public/invoices/' . $attributes['pdf_file']);
        
        return redirect ('/invoices')->with('success', __('invoices.invoice_added'));

    }

    public function show(Invoice $invoice)
    {
        abort_if(Gate::denies('update', $invoice), 404);
        if(!Storage::exists('invoices/' . $invoice->pdf_file)) 
            return redirect('/invoices/' . $invoice->id . '/edit')->with('error', __('invoices.pdf_not_found'));

        return response()->file(storage_path() . '/app/public/invoices/' . $invoice->pdf_file);
    }

    public function edit(Invoice $invoice)
    {
        abort_if(Gate::denies('update', $invoice) || !$invoice->customer, 404);
        $customers = Auth::user()->customers;
        $articles = Article::all();
        $invoice_articles = $invoice->articles;
        return view('invoices.edit', compact('invoice', 'customers', 'articles', 'invoice_articles'));
    }

    public function update(Request $request, Invoice $invoice)
    {
    	$user = User::findOrFail(Auth::user()->id);
        $attributes = $this->validateInvoice($current_invoice_number = $invoice->invoice_number, $user);
        $attributes['user_id'] = $user->id;
        $attributes['total'] = 0;
        $attributes['discount'] = 0;
        (request('tax-free-invoice') === 'true') ? 
            $attributes['tax'] = 0 : $attributes['tax'] = 20;
        $attributes['pdf_file'] = 'Invoice_' . time() . '.pdf';

        //Calculate sum for invoice
        $articles = json_decode($attributes['articleslist'], true);
        foreach ($articles as $article) {
            $attributes['total'] += $article['total'];
        }
        $attributes['total'] = number_format((float)$attributes['total'], 2, '.', '');
        $attributes['tax_value'] = ($attributes['total'] * ($attributes['tax']/100));
        $attributes['tax_value'] = number_format((float)$attributes['tax_value'], 2, '.', '');
        $attributes['grand_total'] = $attributes['total'] + $attributes['tax_value'];
        $attributes['grand_total'] = number_format((float)$attributes['grand_total'], 2, '.', '');

        //Add fields for articles which are not created automatically
        $articles = collect($articles)->map(function ($item) use($invoice) {
            $item['invoice_id'] = $invoice->id;
            $item['created_at'] = now()->toDateString();
            $item['updated_at'] = now()->toDateString();
            return $item;
        });

        //Remove all articles for current invoice
        InvoiceArticle::where('invoice_id', $invoice->id)->delete();

        //Remove PDF file
        if(Storage::exists('invoices/' . $invoice->pdf_file))
            Storage::delete('invoices/' . $invoice->pdf_file);

        //Update invoice
        $invoice->update($attributes);

        //Insert new articles
        InvoiceArticle::insert($articles->toArray());

        //Recreate PDF file
        $user_settings = $user->user_settings;
        $pdf = PDF::loadView('common.templates.invoice_template1', compact('invoice', 'articles', 'user_settings'));
        $pdf->save(storage_path() .'/app/public/invoices/' . $attributes['pdf_file']);
        
        return redirect ('/invoices')->with('success', __('invoices.offer_updated'));

    }

    public function destroy(Invoice $invoice)
    {
        abort_if(Gate::denies('update', $invoice), 404);
        $invoice->delete();

        //Remove PDF
        if(Storage::exists('invoices/' . $invoice->pdf_file))
            Storage::delete('invoices/' . $invoice->pdf_file);

        return redirect ('/invoices')->with('success', __('invoices.invoice_deleted'));
    }

    protected function validateinvoice($current_invoice_number, $user)
    {
        $invoice_numbers = '';
        if($user->invoices) {
            $invoice_numbers = $user->invoices->pluck('invoice_number')->toArray();
            if($_SERVER['REQUEST_URI'] != '/invoices') { //edit invoice so we can use existing invoice number as well
                $key = array_search($current_invoice_number, $invoice_numbers);
                $invoice_numbers = Arr::except($invoice_numbers, [$key]);
            }
            $invoice_numbers = implode(',', $invoice_numbers);
        }
        return request()->validate([
            'invoice_number' => ['not_in:'.$invoice_numbers,'required','string','min:8','max:15'],
            'customer_id' => ['required'],
            'invoice_date' => ['required','date'],
            'articleslist' => ['required']
        ]);
    }

    protected function generateInvoiceNumber($user)
    {
        $prefix = ($user->offer_invoice_settings->invoice_prefix) ?? 'RN';
        $delimiter = ($user->offer_invoice_settings->invoice_delimiter) ?? '/';
        $suffix = ($user->offer_invoice_settings->invoice_suffix) ?? date('Y');
        $invoice_numbers = array();

        if($user->invoices) {
            foreach($user->invoices as $invoice) {
            	$temp_number = substr($invoice->invoice_number, 2);
            	$temp_number = substr($temp_number, 0, -5);
                array_push($invoice_numbers, $temp_number);
            }
        }
        if(empty($invoice_numbers))
            return $prefix . '0001' . $delimiter . $suffix;
        else {
            $counter = max($invoice_numbers);
            $counter++;
            switch (true) {
                case $counter < 10: 
                    $number = $prefix . '000' . $counter . $delimiter . $suffix;
                    break;
                case $counter < 100:
                    $number = $prefix . '00' . $counter . $delimiter . $suffix;
                    break;
                case $counter < 1000:
                    $number = $prefix . '0' . $counter . $delimiter . $suffix;
                    break;
                case $counter >= 1000:
                    $number = $prefix . $counter . $delimiter . $suffix;
                    break;
                default:
                    $number = $prefix . rand() . $delimiter . $suffix;
                    break;
            }
            return $number;
        }
    }
}
