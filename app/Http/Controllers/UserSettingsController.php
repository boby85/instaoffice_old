<?php

namespace App\Http\Controllers;

use App\User;
use App\UserSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class UserSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
        $user_settings = $user->user_settings;
        $logo_path = ($user_settings->company_logo) ? 
            'storage/user_logo/' . $user_settings->company_logo : 
            'storage/user_logo/fallback.png';

        return view ('user_settings.index', compact('user_settings', 'logo_path'));
    }

    public function update(Request $request, UserSettings $userSettings)
    {
        $attributes['user_id'] = Auth::user()->id;
        $attributes = $this->validateGeneralSettings();    
        if(request('company_logo'))
            $attributes['company_logo'] = $this->createLogoImage(request('company_logo'));

        UserSettings::where('user_id', Auth::user()->id)->update($attributes);

        //$lang = Auth::user()->user_settings->language;
        $new_language = $attributes['language'];

        return redirect (url('locale/' . $new_language))->with('success', __('user_settings.settings_saved'))->cookie('lang', $new_language, 525600);

    }

    protected function validateGeneralSettings()
    {
        return request()->validate([
            'company_name' => ['string','min:3','max:50','nullable'],
            'name' => ['required','string','min:2','max:30'],
            'address' => ['required','string','min:5','max:50'],
            'city' => ['required','string','min:3','max:30'],
            'zip_code' => ['required','string','min:3','max:10'],
            'country' => ['required','string','min:3','max:30'],
            'phone' => ['required','digits_between:6,20'],
            'website' => ['string','min:5','max:100','nullable'],
            'company_logo' => ['image', 'mimes:jpeg,png,jpg,gif,svg','max:8192',
                        'dimensions:min_width=80, min_height=80',
                        'dimensions:max_width=4000, max_height=3000'],
            'company_uid_number' => ['string','min:3','max:15','nullable'],
            'company_tax_number' => ['string','min:3','max:15','nullable'],
            'company_register_number' => ['string','min:3','max:15','nullable'],
            'bank_name' => ['string','min:3','max:30','nullable'],
            'bank_bic' => ['string','min:11','max:11','nullable'],
            'bank_iban' => ['string','min:20','max:24','nullable'],
            'language' => ['in:en,de']

        ]);
    }

    protected function createLogoImage($image)
    {
        $imageName = 'user_logo_' . time() . '.' . $image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());
        $image_resize->resize(null, 90, function ($constraint) {
            $constraint->aspectRatio();
        });

        if(!Storage::put('user_logo/' . $imageName, (string) $image_resize->encode()))
        {
            return back()->with('error', 'Image upload problem.');   
        } else {
            return $imageName;
        }
    }
}