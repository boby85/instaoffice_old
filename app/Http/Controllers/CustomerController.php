<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
        $customers = $user->customers;
        return view ('customers.index', compact('customers'));
    }

    public function create()
    {
        $customer_number = $this->generateCustomerNumber();
        return view('customers.create', compact('customer_number'));
    }

    public function store(Request $request)
    {
        $attributes = $this->validateCustomer($current_customer_email = null, $current_customer_number = null);
        $attributes['user_id'] = Auth::user()->id;
        $customer = Customer::create($attributes);

        return redirect ('/customers')->with('success', __('customers.customer_added') ); 
    }

    public function show(Customer $customer)
    {
        abort_if(Gate::denies('update', $customer), 404);
        $offers = $customer->offers;
        $invoices = $customer->invoices;
        return view('customers.show', compact('customer', 'offers', 'invoices'));   
    }

    public function edit(Customer $customer)
    {
        abort_if(Gate::denies('update', $customer), 404);
        return view('customers.edit', compact('customer'));
    }

    public function update(Request $request, Customer $customer)
    {
        abort_if(Gate::denies('update', $customer), 404);
        $attributes = $this->validateCustomer(
            $current_customer_email = $customer->email, 
            $current_customer_number = $customer->customer_number
        );
        $customer->update($attributes);

        return redirect ('/customers')->with('success', __('customers.customer_updated') ); 
    }

    public function destroy(Customer $customer)
    {
        abort_if(Gate::denies('update', $customer), 404);
        $customer->delete();

        return redirect ('/customers')->with('success', __('customers.customer_deleted') );
    }

    protected function validateCustomer($current_customer_email, $current_customer_number)
    {
        $user = User::findOrFail(Auth::user()->id);
        $customer_emails = '';
        $customer_numbers = '';
        if($user->customers) {
            $customer_emails = $user->customers->pluck('email')->toArray();
            $customer_numbers = $user->customers->pluck('customer_number')->toArray();
            if($_SERVER['REQUEST_URI'] != '/customers') { //edit customer so we can use existing email and number as well
                $key = array_search($current_customer_email, $customer_emails); 
                $customer_emails = Arr::except($customer_emails, [$key]);
                $key = array_search($current_customer_number, $customer_numbers);
                $customer_numbers = Arr::except($customer_numbers, [$key]);
            }
            $customer_emails = implode(',', $customer_emails);
            $customer_numbers = implode(',', $customer_numbers);
        }

        return request()->validate([
            'customer_number' => ['not_in:'.$customer_numbers,'required','digits_between:4,6'],
            'company_name' => ['string','min:3','max:50','nullable'],
            'title' => ['required','string','min:2','max:2'],
            'name' => ['required','string','min:3','max:60'],
            'address' => ['required','string','min:5','max:50'],
            'zip_code' => ['required','string','min:3','max:10'],
            'city' => ['required','string','min:3','max:30'],
            'country' => ['required','string','min:3','max:30'],
            'company_tax_number' => ['string', 'min:5','max:20','nullable'],
            'phone' => ['required','digits_between:6,20'],
            'mobile_phone' => ['digits_between:6,20','nullable'],
            'fax' => ['digits_between:6,20','nullable'],
            'email' => ['not_in:'.$customer_emails,'required','email','min:5','max:100'],
            'website' => ['active_url','min:5','max:100','nullable']
        ]);
    }

    protected function generateCustomerNumber()
    {
        $user = User::findOrFail(Auth::user()->id);
        $customer_numbers = array();
        if($user->customers) {
            foreach($user->customers as $customer)
                array_push($customer_numbers, $customer->customer_number);
        }
        if(empty($customer_numbers))
            return '0001';
        else {
            $counter = max($customer_numbers);
            $counter++;
            switch (true) {
                case $counter < 10: 
                    $code = '000' . $counter;
                    break;
                case $counter < 100:
                    $code = '00' . $counter;
                    break;
                case $counter < 1000:
                    $code = '0' . $counter;
                    break;
                case $counter >= 1000:
                    $code = $counter;
                    break;
                default:
                    $code = rand();
                    break;
            }
            return $code;
        }
    }
}