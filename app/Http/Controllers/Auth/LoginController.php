<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\Request;
use Illuminate\Validation\ValidationException;
use App\Http\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated()
    {
        if (!Auth::user()->active) {
            Auth::logout();
            return redirect('/login')->withError(__('auth.account_inactive'));
        }
        return redirect($this->redirectPath());
    }

    protected function redirectPath()
    {
        $path = '/login';
        $role = Auth::user()->role->role_name;
        switch($role) {
            case 'admin':
                $path = '/users';
                break;
            case 'user':
                $path = '/dashboard';
                break;
            default:
                $path = '/login';
                break;
        }
        return $path;
    }
}