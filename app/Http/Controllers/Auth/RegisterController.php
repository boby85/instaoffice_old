<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\UserSettings;
use App\Role;
use App\OfferInvoiceSettings;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $redirectTo = '/login';


    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required','string','min:2','max:30'],
            'company_name' => ['string','min:3','max:50','nullable'],
            'email' => ['required', 'string', 'email','min:5','max:100','unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required','digits_between:6,20'],
            'address' => ['required','string','min:5','max:50'],
            'zip_code' => ['required','string','min:3','max:10'],
            'city' => ['required','string','min:3','max:30'],
            'country' => ['required','string','min:3','max:30']
        ]);
    }

    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'company_name' => $data['company_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
            'active' => true
        ]);

        UserSettings::create([
            'user_id' => $user->id,
            'name' => $data['name'],
            'company_name' => $data['company_name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'zip_code' => $data['zip_code'],
            'city' => $data['city'],
            'country' => $data['country'],
            'language' => 'en'
        ]);

        Role::create([
            'user_id' => $user->id,
            'role_name' => 'user'
        ]);

        OfferInvoiceSettings::create([
            'user_id' => $user->id,
            'offer_prefix' => 'AN',
            'offer_suffix' => date('Y'),
            'invoice_prefix' => 'RN',
            'invoice_suffix' => date('Y'),
        ]);

        return $user;
    }
}
