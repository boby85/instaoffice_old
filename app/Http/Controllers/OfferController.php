<?php

namespace App\Http\Controllers;

use App\Offer;
use App\User;
use App\Article;
use App\OfferArticle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Arr;

class OfferController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
        $offers = $user->offers;
        
        return view('offers.index', compact('offers'));
    }

    public function create()
    {
        $user = User::findOrFail(Auth::user()->id);
        $customers = $user->customers;
        if(!$customers->count())
            return back()->with('error', __('offers.no_customers'));
        $offer_number = $this->generateOfferNumber();
        $articles = Article::all();

        return view('offers.create', compact('customers', 'offer_number', 'articles'));
    }

    public function store(Request $request)
    {
        $attributes = $this->validateOffer($current_offer_number = null);
        $attributes['user_id'] = Auth::user()->id;
        $attributes['total'] = 0;
        $attributes['discount'] = 0;
        (request('tax-free-offer') === 'true') ? 
            $attributes['tax'] = 0 : $attributes['tax'] = 20;
        $attributes['pdf_file'] = 'Offer_' . time() . '.pdf';

        //Calculate sum for offer
        $articles = json_decode($attributes['articleslist'], true);
        foreach ($articles as $article) {
            $attributes['total'] += $article['total'];
        }
        $attributes['total'] = number_format((float)$attributes['total'], 2, '.', '');
        $attributes['tax_value'] = ($attributes['total'] * ($attributes['tax']/100));
        $attributes['tax_value'] = number_format((float)$attributes['tax_value'], 2, '.', '');
        $attributes['grand_total'] = $attributes['total'] + $attributes['tax_value'];
        $attributes['grand_total'] = number_format((float)$attributes['grand_total'], 2, '.', '');

        //Create offer
        $offer = Offer::create($attributes);

        //Add fields for articles which are not created automatically
        $articles = collect($articles)->map(function ($item) use($offer) {
            $item['offer_id'] = $offer->id;
            $item['created_at'] = now()->toDateString();
            $item['updated_at'] = now()->toDateString();
            return $item;
        });

        //Insert articles
        OfferArticle::insert($articles->toArray());

        //Generate PDF
        $user_settings = Auth::user()->user_settings;
        $pdf = PDF::loadView('common.templates.offer_template1', compact('offer', 'articles', 'user_settings'));
        //return $pdf->stream();
        $pdf->save(storage_path() .'/app/public/offers/' . $attributes['pdf_file']);
        
        return redirect ('/offers')->with('success', __('offers.offer_added'));
    }

    public function show(Offer $offer)
    {
        abort_if(Gate::denies('update', $offer), 404);
        if(!Storage::exists('offers/' . $offer->pdf_file)) 
            return redirect('/offers/' . $offer->id . '/edit')->with('error', __('offers.pdf_not_found'));

        return response()->file(storage_path() . '/app/public/offers/' . $offer->pdf_file);
    }

    public function edit(Offer $offer)
    {
        abort_if(Gate::denies('update', $offer) || !$offer->customer, 404);
        $customers = Auth::user()->customers;
        $articles = Article::all();
        $offer_articles = $offer->articles;
        return view('offers.edit', compact('offer', 'customers', 'articles', 'offer_articles'));
    }

    public function update(Request $request, Offer $offer)
    {
        $attributes = $this->validateOffer($current_offer_number = $offer->offer_number);
        $attributes['total'] = 0;
        $attributes['discount'] = 0;
        (request('tax-free-offer') === 'true') ? 
            $attributes['tax'] = 0 : $attributes['tax'] = 20;
        $attributes['pdf_file'] = 'Offer_' . time() . '.pdf';

        //Calculate sum for offer
        $articles = json_decode($attributes['articleslist'], true);
        foreach ($articles as $article) {
            $attributes['total'] += $article['total'];
        }
        $attributes['total'] = number_format((float)$attributes['total'], 2, '.', '');
        $attributes['tax_value'] = ($attributes['total'] * ($attributes['tax']/100));
        $attributes['tax_value'] = number_format((float)$attributes['tax_value'], 2, '.', '');
        $attributes['grand_total'] = $attributes['total'] + $attributes['tax_value'];
        $attributes['grand_total'] = number_format((float)$attributes['grand_total'], 2, '.', '');

        //Add fields for articles which are not created automatically
        $articles = collect($articles)->map(function ($item) use($offer) {
            $item['offer_id'] = $offer->id;
            $item['created_at'] = now()->toDateString();
            $item['updated_at'] = now()->toDateString();
            return $item;
        });      

        //Remove all articles for current invoice
        OfferArticle::where('offer_id', $offer->id)->delete();

        //Remove PDF file
        if(Storage::exists('offers/' . $offer->pdf_file))
            Storage::delete('offers/' . $offer->pdf_file);

        //Update offer
        $offer->update($attributes);

        //Insert new articles
        OfferArticle::insert($articles->toArray());

        //Recreate PDF file
        $user_settings = Auth::user()->user_settings;
        $pdf = PDF::loadView('common.templates.offer_template1', compact('offer', 'articles', 'user_settings'));
        $pdf->save(storage_path() .'/app/public/offers/' . $attributes['pdf_file']);
        
        return redirect ('/offers')->with('success', __('offers.offer_updated'));
    }

    public function destroy(Offer $offer)
    {
        abort_if(Gate::denies('update', $offer), 404);
        $offer->delete();

        //Remove PDF
        if(Storage::exists('offers/' . $offer->pdf_file))
            Storage::delete('offers/' . $offer->pdf_file);

        return redirect ('/offers')->with('success', __('offers.offer_deleted'));
    }

    protected function validateOffer($current_offer_number)
    {
        $user = User::findOrFail(Auth::user()->id);
        $offer_numbers = '';
        if($user->offers) {
            $offer_numbers = $user->offers->pluck('offer_number')->toArray();
            if($_SERVER['REQUEST_URI'] != '/offers') { //edit offer so we can use existing offer number as well
                $key = array_search($current_offer_number, $offer_numbers);
                $offer_numbers = Arr::except($offer_numbers, [$key]);
            }
            $offer_numbers = implode(',', $offer_numbers);
        }
        return request()->validate([
            'offer_number' => ['not_in:'.$offer_numbers,'required','string','min:8','max:15'],
            'customer_id' => ['required'],
            'offer_date' => ['required','date'],
            'offer_expire_date' => ['required', 'date','after_or_equal:start_date'],
            'articleslist' => ['required']
        ]);
    }

    protected function generateOfferNumber()
    {
        $user = User::findOrFail(Auth::user()->id);
        $prefix = ($user->offer_invoice_settings->offer_prefix) ?? 'AN';
        $delimiter = ($user->offer_invoice_settings->offer_delimiter) ?? '/';
        $suffix = ($user->offer_invoice_settings->offer_suffix) ?? date('Y');
        $offer_numbers = array();
        if($user->offers) {
            foreach($user->offers as $offer) {
            	$temp_number = substr($offer->offer_number, 2);
            	$temp_number = substr($temp_number, 0, -5);
                array_push($offer_numbers, $temp_number);
            }
        }
        if(empty($offer_numbers))
            return $prefix . '0001' . $delimiter . $suffix;
        else {
            $counter = max($offer_numbers);
            $counter++;
            switch (true) {
                case $counter < 10: 
                    $number = $prefix . '000' . $counter . $delimiter . $suffix;
                    break;
                case $counter < 100:
                    $number = $prefix . '00' . $counter . $delimiter . $suffix;
                    break;
                case $counter < 1000:
                    $number = $prefix . '0' . $counter . $delimiter . $suffix;
                    break;
                case $counter >= 1000:
                    $number = $prefix . $counter . $delimiter . $suffix;
                    break;
                default:
                    $number = $prefix . rand() . $delimiter . $suffix;
                    break;
            }
            return $number;
        }
    }

    protected function getArticleData($article_id) 
    {
        if($article_id) {
            $article = Article::findOrFail($article_id);
            $attributes['main_product_group'] = ($article->main_product_group) ?? '-';
            $attributes['price'] = ($article->price) ?? '0.00';
            $attributes['price_unit'] = ($article->price_unit) ?? '1';
            $attributes['unit'] = ($article->unit) ?? 'ST';
            $attributes['currency'] = ($article->currency) ?? 'EUR';
            $attributes['image_name'] = ($article->articleImage) ? $article->articleImage->name : 'no_image.png';
        }
       return json_encode(array('data' => $attributes));
       
    }

}
