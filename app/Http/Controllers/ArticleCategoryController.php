<?php

namespace App\Http\Controllers;

use App\ArticleCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Article;

class ArticleCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $article_categories = ArticleCategory::all();
        return view ('article_categories.index', compact('article_categories'));
    }

    public function create()
    {
        $next_id = ArticleCategory::max('category_id') + 1;
        return view('article_categories.create', compact('next_id'));
    }

    public function store(Request $request)
    {
        $attributes = $this->validateCategory(null);
        $article_category = ArticleCategory::create($attributes);

        return redirect ('/article_categories')->with('success', __('article_categories.article_category_added') ); 
    }

    public function edit(ArticleCategory $articleCategory)
    {
        $count = Article::where('category', $articleCategory->category_id)->count();
        return view('article_categories.edit', compact('articleCategory', 'count'));
    }

    public function update(Request $request, ArticleCategory $articleCategory)
    {
        $attributes = $this->validateCategory($articleCategory);
        $articleCategory->update($attributes);

        return redirect ('/article_categories')->with('success', __('article_categories.category_updated') ); 
    }

     public function destroy(ArticleCategory $articleCategory)
    {
        $articleCategory->delete();

        return redirect ('/article_categories')->with('success', __('article_categories.category_deleted') );
    }

    protected function validateCategory($articleCategory)
    {	
    	$ids = $this->getAllowedIds();
		$ids = implode(',', $ids);

        return request()->validate([
            'category_name' => ['required','string','min:1','max:100','unique:article_categories'],
            'category_id' => [
            	'required',
            	'numeric',
            	Rule::unique('article_categories')->ignore($articleCategory->category_id, 'category_id')
            ],
            'category_parent_id' => ['required', 'in:'.$ids,'numeric']
        ]);
    }

    protected function getAllowedIds() //Get all existing category IDs, plus root (zero) ID
    {
    	$category_parents_ids = array(0);
		if(ArticleCategory::pluck('category_id')->count()) {
			$category_parents_ids = ArticleCategory::pluck('category_id')->unique()->toArray();
			array_push($category_parents_ids, 0);
		}

		return $category_parents_ids;
    }
}
