<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\UserSettings;
use App\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::all();
        return view ('users.index', compact('users'));
    }

    public function create()
    {
        return back()->with('error', 'Use register form to add new user');
    }

    public function store(Request $request)
    {
        abort(404);
    }

    public function show(User $user)
    {
        return view('users.show', compact('user'));   
    }

    public function edit(User $user)
    {
        abort(404);
    }

    public function update(Request $request, $id)
    {
        if(!request('action'))
            return back()->with('error', 'Missing required parametar action.');

        $user = User::findOrFail($id);
        if($user->role->role_name === 'admin')
            return back()->with('error', 'Can\'t change admininstrator user.');

        $attributes['active'] = (request('action') ===  'activate');
        $user->update($attributes);
        
        return redirect ('/users')->with('success', __('users.user_updated') );

    }

    public function destroy($id)
    {
        if($id == 1)
            return back()->with('error', 'Can\'t delete admininstrator user.');
        
        $user = User::findOrFail($id);
        $user->delete();
        return redirect ('/users')->with('success', __('users.user_deleted') );
    }
    
}