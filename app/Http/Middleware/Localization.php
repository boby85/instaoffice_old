<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! Auth::check() && (Cookie::get('lang') !== null)) {
            app()->setLocale(Cookie::get('lang'));
        }
        
        $locale = Auth::check() 
            ? auth()->user()->user_settings->language
            : config('app.locale');          
        app()->setLocale($locale);
        
        return $next($request);
    }
}
