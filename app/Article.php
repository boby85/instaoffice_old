<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
    	'code', 'name', 'description', 'category', 'price', 'price_unit', 
    	'unit', 'currency', 'net_weight', 'weight_unit', 'series'
    ];

    public function articleImage()
    {
    	return $this->hasOne('App\ArticleImage');
    }

    public function category()
   	{
   		return $this->belongsTo('App\ArticleCategory');
   	}
}
