<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'company_name', 'email', 'password', 'active',
        'phone', 'address', 'zip_code', 'city', 'country'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->hasOne('App\Role');
    }

    public function user_settings() 
    {
        return $this->hasOne('App\UserSettings');
    }

    public function offer_invoice_settings()
    {
        return $this->hasOne('App\OfferInvoiceSettings');
    }

    public function customers()
    {
        return $this->hasMany('App\Customer');
    }

    public function offers()
    {
        return $this->hasMany('App\Offer');
    }

    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }
}
