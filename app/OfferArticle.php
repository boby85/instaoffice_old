<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferArticle extends Model
{
    use HasFactory;

    protected $fillable = [
    	'offer_id', 'article_id', 'image_path', 'name', 'quantity', 'price', 'total'
    ];

    public function offer()
    {
        return $this->belongsTo('App\Offer');
    }
}
