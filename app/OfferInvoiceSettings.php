<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferInvoiceSettings extends Model
{
    use HasFactory;

    protected $fillable = [
    	'user_id', 'offer_prefix', 'offer_suffix', 'offer_delimiter',
    	'invoice_prefix', 'invoice_suffix', 'invoice_delimiter'
    ];

    public function user()
    {
    	return belongsTo('App\User');
    }
}
