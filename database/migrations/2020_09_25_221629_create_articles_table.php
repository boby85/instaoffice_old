<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id('id');
            $table->string('code')->unique();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('category')->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->integer('price_unit')->default(1);
            $table->enum('unit', ['ST', 'M', 'M2', 'M3', 'KG', 'L', 'other'])->default('ST');
            $table->enum('currency', ['EUR', 'USD'])->default('EUR');
            $table->string('main_product_group')->nullable();
            $table->decimal('net_weight', 10, 3)->nullable();
            $table->enum('weight_unit', ['G', 'KG'])->default('G');
            $table->string('series')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
