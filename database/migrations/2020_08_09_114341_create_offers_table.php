<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('customer_id')->index();
            $table->string('offer_number');
            $table->date('offer_date');
            $table->date('offer_expire_date');
            $table->decimal('total', 10, 2);
            $table->decimal('discount',5, 2);
            $table->decimal('tax',5, 2);
            $table->decimal('tax_value', 10, 2);
            $table->decimal('grand_total', 10, 2);
            $table->string('pdf_file');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
