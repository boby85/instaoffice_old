<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferInvoiceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_invoice_settings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('offer_prefix')->default('AN');
            $table->year('offer_suffix')->default(2021);
            $table->string('offer_delimiter')->default('/');
            $table->string('offer_template_name')->nullable()->default(NULL);
            $table->string('invoice_prefix')->default('RN');
            $table->year('invoice_suffix')->default(2021);
            $table->string('invoice_delimiter')->default('/');
            $table->string('invoice_template_name')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_invoice_settings');
    }
}
