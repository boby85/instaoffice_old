/***Validation ***/
$("#invoice-form").validate({
    errorClass: "invoice-form-error-class",
    rules: {
        invoice_number: {
            required: true,
            minlength: 8,
            maxlength: 15
        },
        customer_id: {
            required: true,
        },
        invoice_date: {
            required: true,
            date: true
        }
    }
});
$("#articles-fields").validate({
    errorClass: "articles-fields-error-class",
    rules: {
        article_select: {
            required: true,
        },
        price: {
            required: true,
            number: true
        },
        quantity: {
            required: true,
            digits: true,
            min: 1,
            max: 10000
        },
        discount: {
            required: true,
            number: true,
            min: 0,
            max: 100
        }
    }
});
$("#custom-insert-modal-form").validate({
    errorClass: "custom-insert-modal-error-class",
    rules: {
        name_modal: {
            required: true,
            minlength: 3,
            maxlength: 100
        },
        price_modal: {
            required: true,
            number: true,
            max: 1000000
        },
        quantity_modal: {
            required: true,
            digits: true,
            min: 1,
            max: 10000
        },
        discount_modal: {
            required: true,
            number: true,
            min: 0,
            max: 100
        }
    }
});
$("#edit-modal-form").validate({
    errorClass: "edit-modal-error-class",
    rules: {
        edit_price: {
            required: true,
            number: true,
            max: 1000000
        },
        edit_quantity: {
            required: true,
            digits: true,
            min: 1,
            max: 10000
        },
        edit_discount: {
            required: true,
            number: true,
            min: 0,
            max: 100
        }
    }
});
/***End Validation ***/

$(".add-article").click(function() {
    if($("#articles-fields").valid()) {
        var article_id = $("#article_select option:selected" ).val();
        article_id = article_id.trim();
        article_id = parseInt(article_id);
        var article_image_path = $('#article-image-preview').attr('src') ;
        article_image_path = article_image_path.trim();
        var article_name = $("#article_select option:selected" ).text();
        article_name = article_name.trim();
        var price = parseFloat($('#price').val());
        price = price.toFixed(2);
        var quantity = $('#quantity').val();
        quantity = parseInt(quantity);     
        var discount = parseFloat($('#discount').val());
        discount = discount.toFixed(2);
        var total = price * quantity;
        total = total - (total*(discount/100));
        total = total.toFixed(2);
        updateSum(total, isTaxFreeSelected());
        $("#invoice-articles-table tbody").append(
            '<tr>' +
                '<input type="hidden" name="article_id" class="tdId" value=' + article_id + '>' +
                '<td class="tdImage"> <img id="invoice-article-image" src=' + article_image_path + ' width="50px" height="auto"></td>' +
                '<td class="tdArticle">' + article_name + '</td>' +
                '<td class="tdPrice">' + price + '</td>' +
                '<td class="tdQuantity">' + quantity + '</td>' +
                '<td class="tdDiscount">' + discount + '</td>' +
                '<td class="tdPriceTotal">' + total +'</td>' +
                '<td>' +
                    '<button type="button" class="btn btnEdit" data-toggle="modal" data-target="#edit-modal">' +
                        '<span class="feather-icons" data-feather="edit"></span>' +
                    '</button>' +
                    '<button type="button" class="btn btnDelete">' +
                        '<span class="feather-icons" data-feather="trash-2"></span>' +
                    '</button>' +
                '</td>' +
            '</tr>'
        );
        feather.replace();

        $("#article_select").val(null).trigger('change');
        $("#price").val('0.00');
        $("#quantity").val(1);
        $("#discount").val('0.00');
        $('#article-image-preview').attr('src', '');
        $('#product_group').empty();
        $('#price_unit').empty();
    }
});

$("#custom-insert-btn-add").click(function() {
    if($("#custom-insert-modal-form").valid()) {
        var name = $("#name_modal").val();
        name = name.trim();
        var price = parseFloat($('#price_modal').val());
        price = price.toFixed(2);
        var quantity = $('#quantity_modal').val();
        quantity = parseInt(quantity);      
        var discount = parseFloat($('#discount_modal').val());
        discount = discount.toFixed(2);
        var total = price * quantity;
        total = total - (total*(discount/100));
        total = total.toFixed(2);
        updateSum(total, isTaxFreeSelected());
        $("#invoice-articles-table tbody").append(
            '<tr>' +
                '<input type="hidden" name="article_id" class="tdId" value="0"' + '>' +
                '<td class="tdImage"> <img id="invoice-article-image" src="../../storage/articles/no_image.png" width="50px" height="auto"></td>' +
                '<td class="tdArticle">' + name + '</td>' +
                '<td class="tdPrice">' + price + '</td>' +
                '<td class="tdQuantity">' + quantity + '</td>' +
                '<td class="tdDiscount">' + discount + '</td>' +
                '<td class="tdPriceTotal">' + total +'</td>' +
                '<td>' +
                    '<button type="button" class="btn btnEdit" data-toggle="modal" data-target="#edit-modal">' +
                        '<span class="feather-icons" data-feather="edit"></span>' +
                    '</button>' +
                    '<button type="button" class="btn btnDelete">' +
                        '<span class="feather-icons" data-feather="trash-2"></span>' +
                    '</button>' +
                '</td>' +
            '</tr>'
        );
        feather.replace();
        $('#custom-insert-modal').modal('hide');
    }
});

$("#custom-insert-modal").on("hidden.bs.modal", function(){
    $("#name_modal").val('');
    $("#price_modal").val('0.00');
    $("#quantity_modal").val(1);
    $("#discount_modal").val('0.00');
});

$("#invoice-articles-table").on('click', '.btnDelete', function () {
    var total = $(this).closest("tr").find(".tdPriceTotal").text();
    updateSum(-total, isTaxFreeSelected());
    $(this).closest('tr').remove();
});

$("#invoice-articles-table").on('click', '.btnEdit', function (e) {
	e.preventDefault();
    //Current row values
    var rowToUpdate = $(this).closest('tr');
    var article = rowToUpdate.find(".tdArticle").text().trim();
    var price = rowToUpdate.find(".tdPrice").text();
    var quantity = rowToUpdate.find(".tdQuantity").text();
    var discount = rowToUpdate.find(".tdDiscount").text();
    var rowTotal = rowToUpdate.find(".tdPriceTotal").text();

    //Populate modal with values
    $('#edit_name', '#edit-modal').val(article);
    $('#edit_price', '#edit-modal').val(price);
    $('#edit_quantity', '#edit-modal').val(quantity);
    $('#edit_discount', '#edit-modal').val(discount);

    //Validate & update data
    $("#editBtnModal").on('click', function() {  
        var editArticleName = $("#edit_name").val();
        var editPrice = parseFloat($('#edit_price').val());
        var editQuantity = $('#edit_quantity').val();       
        var editDiscount = parseFloat($('#edit_discount').val());

        if($("#edit-modal-form").valid()) {
            editPrice = editPrice.toFixed(2);
            editDiscount = editDiscount.toFixed(2);
            var editTotal = editPrice * editQuantity;
            editTotal = editTotal - (editTotal*(editDiscount/100));
            editTotal = editTotal.toFixed(2);
            
	        rowToUpdate.find(".tdPrice").text(editPrice);
	        rowToUpdate.find(".tdQuantity").text(editQuantity);
	        rowToUpdate.find(".tdDiscount").text(editDiscount);
	        rowToUpdate.find(".tdPriceTotal").text(editTotal);
	        updateSum(editTotal - rowTotal, isTaxFreeSelected());
	        $("#editBtnModal").off();
	        $('#edit-modal').modal('hide');
    	}
    });
});

$('#invoice-articles-table').one('hide.bs.modal', function () {
	$(this).off('hide.bs.modal');
});

$("#submit-invoice-button").click(function( event ) {
    if($("#invoice-form").valid()) {
        $('.loading').fadeIn();
        var articles = collectArticles();
        var data = JSON.stringify(articles);
        $('#articleslist').val(data);
        $('#tax-free-invoice').val(isTaxFreeSelected());
        $('#invoice-form').submit();
    }
});

$('#tax-free').change(function(){
    (this.checked) ? updateSum(0, true) : updateSum(0, false);
    (this.checked) ? $('.tax-20').text('0%') : $('.tax-20').text('20%');
});

function collectArticles() {
    var articles = [];
    $("#invoice-articles-table tbody tr").each(function () {
        aId = $(this).find('.tdId').val();
        aId = aId.trim();
        aImagePath = $(this).find('#invoice-article-image').attr('src');
        aImagePath = aImagePath.trim();
        aImageName = aImagePath.replace(/^.*[\\\/]/, '');
        aName = $(this).find('.tdArticle').text();
        aName = aName.trim();
        aPrice = $(this).find('.tdPrice').text();
        aPrice = aPrice.trim();
        aQuantity = $(this).find('.tdQuantity').text();
        aQuantity = aQuantity.trim();
        aDiscount = $(this).find('.tdDiscount').text();
        aDiscount = aDiscount.trim();
        aTotal = $(this).find('.tdPriceTotal').text();
        aTotal = aTotal.trim();
        articles.push({
            "article_id" : aId,
            "image_name" : aImageName,
            "name" : aName,
            "price" : aPrice,
            "quantity" : aQuantity,
            "discount" : aDiscount,
            "total" : aTotal
        });          
    });
return articles;
}

function updateSum(value, taxFree) {
    var currentTotal = $('#total').text();
    var currentGrandTotal = $('#grandTotal').text();
    var tax = (!taxFree) ? 20.00 : 0; 
    tax = parseFloat(tax);
    tax = tax.toFixed(2);
    var total = parseFloat(currentTotal) + parseFloat(value);
    total = total.toFixed(2);
    var taxValue = parseFloat(total*(tax/100));
    taxValue = taxValue.toFixed(2);
    var grandTotal = parseFloat(total) + parseFloat(taxValue);
    grandTotal = grandTotal.toFixed(2);
    $('#total').html(total);
    $('#tax').html(taxValue);
    $('#grandTotal').html(grandTotal);
}

function isTaxFreeSelected() {
    return ($('#tax-free').is(':checked')) ? true : false
}

$('#article_select').on('change', function() {
    var id = $(this).find(':selected').val();
    if(id != 0) {
	    $.ajaxSetup({
		   	headers: {
	            	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        	}
	   	});
	    $.ajax({
	        url: '/get-article-data/' + id,
	        type: "post",
	        cache: false,
	        dataType: 'json',
	        success:function(resultData){
	           	$('#article-image-preview').attr('src', '../../storage/articles/' + resultData.data.image_name);
	            if (window.location.href.indexOf("invoices/create") > -1)
	            	$('#article-image-preview').attr('src', '../storage/articles/' + resultData.data.image_name);
	            $('#product_group').text(resultData.data.main_product_group);
	            $('#price_unit').text(resultData.data.price + ' ' + resultData.data.currency + ' ' + '(' + resultData.data.price_unit + '/' + resultData.data.unit + ')');
	        },
	    });
	}   
});